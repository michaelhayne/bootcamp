﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarDealership.Models;

namespace CarDealership.Repositories
{
    public class EntityFrameworkCarRepository : ICarRepository
    {
        public void AddCar(Car car)
        {
            using (var context = new CarDBEntities())
            {
                EntityCar entCar = new EntityCar()
                {
                    Make = car.Make,
                    Model = car.Model,
                    Year = car.Year,
                    Title = car.Title,
                    Description = car.Description,
                    Price = (decimal)car.Price
                };
                if (car.ImageUrl == null)
                {
                    entCar.ImageUrl = @"http://icons.iconarchive.com/icons/graphicloads/100-flat-2/256/car-icon.png";
                }
                else
                {
                    entCar.ImageUrl = car.ImageUrl;
                }
                context.EntityCars.Add(entCar);
                context.SaveChanges();
            }
        }

        public void DeleteCar(int carId)
        {
            using (var context = new CarDBEntities())
            {
                var entCar = context.EntityCars.SingleOrDefault(x => x.Id == carId);
                context.EntityCars.Remove(entCar);
                context.SaveChanges();
            }
        }

        public void EditCar(Car modCar)
        {
            using (var context = new CarDBEntities())
            {
                var entCar = context.EntityCars.SingleOrDefault(x => x.Id == modCar.Id);

                entCar.Id = modCar.Id;
                entCar.Make = modCar.Make;
                entCar.Model = modCar.Model;
                entCar.Year = modCar.Year;

                if (modCar.ImageUrl == null)
                {
                    entCar.ImageUrl = @"http://icons.iconarchive.com/icons/graphicloads/100-flat-2/256/car-icon.png";
                }
                else
                {
                    entCar.ImageUrl = modCar.ImageUrl;
                }

                entCar.Title = modCar.Title;
                entCar.Description = modCar.Description;
                entCar.Price = (decimal)modCar.Price;

                context.Entry(entCar).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public List<Car> GetAllCars()
        {
            var carList = new List<Car>();
            using (var context = new CarDBEntities())
            {
                var EntityCarSet = context.EntityCars;
                foreach (var entityCar in EntityCarSet)
                {
                    Car newCar = entityCar;
                    carList.Add(newCar);
                }
            }
            return carList;
        }

        public Car GetCarById(int id)
        {
            Car detailCar = new Car();
            using (var context = new CarDBEntities())
            {
                detailCar = context.EntityCars.SingleOrDefault(x => x.Id == id);
            }
            return detailCar;
        }

        public Car GetCarByModel(string name)
        {
            Car getCar = new Car();
            using (var context = new CarDBEntities())
            {
                getCar = context.EntityCars.SingleOrDefault(x => x.Model == name);
            }
            return getCar;
        }

        public Car GetCarByYearMakeModel(string year, string make, string model)
        {
            Car getCar = new Car();
            using (var context = new CarDBEntities())
            {
                getCar = context.EntityCars.SingleOrDefault(x => (x.Year == year) && (x.Make == make) && (x.Model == model));
            }
            return getCar;
        }

        public User LoginUser(string username, string password)
        {
            throw new NotImplementedException();
        }
    }
}