﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class Car
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Make required")]
        [StringLength(16, ErrorMessage = "Make must be less than 17 characters")]
        public string Make { get; set; }

        [Required(ErrorMessage = "Model required")]
        [StringLength(16, ErrorMessage = "Model must be less than 17 characters")]
        public string Model { get; set; }

        [Required(ErrorMessage = "Year required")]
        [RegularExpression(@"^\d{4}$", ErrorMessage = "Invalid year format")]
        public string Year { get; set; }

        public string ImageUrl { get; set; }

        [Required(ErrorMessage = "Title required")]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Price Required")]
        [Range(0.00, 1000000.00, ErrorMessage = "Price cannot be negative or larger than $1,000,000")]
        public decimal? Price { get; set; }

        public static implicit operator Car(EntityCar v)
        {
            return new Car
            {
                Id = v.Id,
                Make = v.Make,
                Model = v.Model,
                Year = v.Year,
                ImageUrl = v.ImageUrl,
                Title = v.Title,
                Description = v.Description,
                Price = v.Price
            };
        }
    }
}