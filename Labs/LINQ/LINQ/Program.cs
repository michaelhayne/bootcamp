﻿using System;
using System.Linq;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            Linq5();
            Console.ReadLine();
        }

        private static void Linq9()
        {
            var numbers = from b in DataLoader.NumbersB
                          from c in DataLoader.NumbersC
                          where b < c
                          select new
                          {
                              b,
                              c
                          };
            foreach(var pair in numbers)
            {
                Console.WriteLine(pair.b + " " + pair.c);
            }
        }

        private static void Linq8()
        {
            var products = DataLoader.LoadProducts();
            var priceReplace = from product in products
                               select new
                               {
                                   ProductName = product.ProductName,
                                   Category = product.Category,
                                   Price = product.UnitPrice
                               };
            foreach(var x in priceReplace)
            {
                Console.WriteLine(x.Price);
            }
        }

        private static void Linq7()
        {
            var products = DataLoader.LoadProducts();
            var evenStock = from product in products
                            where (product.UnitsInStock % 2) == 0
                            select product;
            foreach (var x in evenStock)
            {
                Console.WriteLine(x.UnitsInStock);
            }
        }

        private static void Linq6()
        {
            var products = DataLoader.LoadProducts();
            var nameList = from product in products
                           select product.ProductName.ToUpper();
            foreach (var x in nameList)
            {
                Console.WriteLine(x);  
            }
        }

        private static void Linq5()
        {
            var products = DataLoader.LoadProducts();

            var things = from product in products
                         select product;

            foreach (var thing in things)
            {
                Console.WriteLine(thing.ProductName + " " + (thing.UnitPrice * 1.25M));
            }

            //foreach (var product in products)
            //{
            //    Console.WriteLine("{0} => {1} {2}", product.UnitPrice, (product.UnitPrice * (decimal)1.25), product.ProductName);
            //}
        }

        private static void Linq3()
        {
            var customers = DataLoader.LoadCustomers();
            var results = customers.Where(c => c.Region == "WA");
            foreach (var customer in results)
            {
                Console.WriteLine("{0}", customer.CompanyName);
                foreach (var order in customer.Orders)
                {
                    Console.WriteLine("   {0} {1} {2}", order.Total, order.OrderDate, order.OrderID);
                }
            }
        }

        private static void Linq2()
        {
            var products = DataLoader.LoadProducts();
            var results = products.Where(p => (0 < p.UnitsInStock && 3 < p.UnitPrice));
            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        private static void Linq4()
        {
            var products = DataLoader.LoadProducts();
            var productNames = from product in products
                               select product.ProductName;
            //foreach (var product in products)
            //{
            //    Console.WriteLine(product.ProductName);
            //}
            foreach (var productName in productNames)
            {
                Console.WriteLine(productName);
            }
        }

        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        // 24. Group customer orders by year, then by month.
        private static void GroupEm()
        {
            var customers = DataLoader.LoadCustomers();
            var result = from customer in customers
                         from order in customer.Orders
                         group order by order.OrderDate.Year into yr
                         select new
                         {
                             Year = yr.Key,
                             MonthGroups = from yrs in yr
                                           group yrs by yrs.OrderDate.Month into mnths
                                           select new { Month = mnths.Key, Orders = mnths}
                         };

            var a = "a";
            
        }
    }
}
