﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarDealership.Models;

namespace CarDealership.Repositories
{
    public class CodeFirstCarRepository : ICarRepository
    {
        public void AddCar(Car car)
        {
            using (var context = new CarContext())
            {
                Car dbCar = new Car()
                {
                    Make = car.Make,
                    Model = car.Model,
                    Year = car.Year,
                    Title = car.Title,
                    Description = car.Description,
                    Price = car.Price
                };
                if (car.ImageUrl == null)
                {
                    dbCar.ImageUrl = @"http://icons.iconarchive.com/icons/graphicloads/100-flat-2/256/car-icon.png";
                }
                else
                {
                    dbCar.ImageUrl = car.ImageUrl;
                }
                context.Cars.Add(dbCar);
                context.SaveChanges();
            }
        }

        public void DeleteCar(int carId)
        {
            throw new NotImplementedException();
        }

        public void EditCar(Car car)
        {
            throw new NotImplementedException();
        }

        public List<Car> GetAllCars()
        {
            var carList = new List<Car>();
            using (var context = new CarContext())
            {
                var carSet = context.Cars;
                foreach (var dbCar in carSet)
                {
                    Car newCar = dbCar;
                    carList.Add(newCar);
                }
            }
            return carList;
        }

        public Car GetCarById(int id)
        {
            throw new NotImplementedException();
        }

        public Car GetCarByModel(string name)
        {
            throw new NotImplementedException();
        }

        public Car GetCarByYearMakeModel(string year, string make, string model)
        {
            throw new NotImplementedException();
        }

        public User LoginUser(string username, string password)
        {
            throw new NotImplementedException();
        }
    }
}