﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using NUnit.Framework;
using BattleShip.UI;

namespace Battleship.Tests
{
    [TestFixture]
    public class ConvertToCoordTests
    {
        [Test]
        public void CoordStringTooLong()
        {
            var response = ConvertToCoord.isValidInput("A001");
            Assert.AreEqual(false, response);
        }
    }
}
