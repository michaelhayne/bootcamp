﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class HighScore
    {
        // Displays the highscores by wins in descending order
        public static void PrintHighScores()
        {
            SortScores();
            Console.WriteLine("\n Name:           Wins:\n ---------------------");
            // Read highscores from text file
            StreamReader streamReader = new StreamReader("HighScores.txt");
            bool isNull = false;
            while (!isNull)
            {
                string txtLine = streamReader.ReadLine();
                if (txtLine == null)
                {
                    isNull = true;
                }
                else
                {
                    // Highscores are stored in the text as a name and number seperated by a space
                    string[] parsedLine = txtLine.Split(' ');
                    Console.WriteLine(" {0,-15} {1}", parsedLine[0], parsedLine[1]);
                }
            }
            streamReader.Close();
            Console.ReadLine();
        }

        // Add one win to a player's score
        public static void AddScore(string name)
        {
            // Read the scores from a text file into a list
            StreamReader streamReader = new StreamReader("HighScores.txt");
            string txtLine = null;
            var txtList = new List<string>();
            string nameLine = null;
            bool searchDone = false;

            // If the highscores file already contains the name, seperate that line out from the list
            while (!searchDone)
            {
                txtLine = streamReader.ReadLine();
                if (txtLine == null)
                {
                    searchDone = true;
                }
                else if (txtLine.Contains(name))
                {
                    nameLine = txtLine;
                }
                else
                {
                    txtList.Add(txtLine);
                }      
            }
            streamReader.Close();

            // Clear the text file
            File.WriteAllText("HighScores.txt", string.Empty);
            StreamWriter streamWriter = new StreamWriter("HighScores.txt", true);
            // If the name was already in the highscores, add one to the number of wins. Otherwise add the name with one win.
            if (nameLine != null)
            {
                string[] lineArray = nameLine.Split(' ');
                int wins = int.Parse(lineArray[1]);
                wins++;
                streamWriter.WriteLine(name + " " + wins);
            }
            else
            {
                streamWriter.WriteLine(name + " " + 1);
            }
            // Write the rest of the highscores back to the tet file
            foreach (var line in txtList)
            {
                streamWriter.WriteLine(line);
            }
            streamWriter.Close();
        }

        // Sort the highscore text file by number of wins
        public static void SortScores()
        {
            // Read the text file into a list of tuples
            StreamReader streamReader = new StreamReader("HighScores.txt");
            var tupleList = new List<Tuple<string, int>>();
            bool isNull = false;
            while (!isNull)
            {
                string line = streamReader.ReadLine();
                if (line == null)
                {
                    isNull = true;
                }
                else
                {
                    tupleList.Add(ParseLine(line));
                }
            }
            streamReader.Close();

            // Sort the list of tuples by wins and then convert that into a list of strings
            var sortedTupleList = from element in tupleList orderby element.Item2 descending select element;
            var sortedStringList = new List<string>();
            foreach (var item in sortedTupleList)
            {
                sortedStringList.Add(item.Item1 + " " + item.Item2.ToString());
            }

            // Clear the highscores text file and then wrie the sorted scores back to the file
            File.WriteAllText("HighScores.txt", string.Empty);
            StreamWriter streamWriter = new StreamWriter("HighScores.txt");
            foreach (var item in sortedStringList)
            {
                streamWriter.WriteLine(item);
            }
            streamWriter.Close();
        }

        // Method to parse a line in the text file into a tuple of the name and the number of wins
        public static Tuple<string, int> ParseLine(string readLine) 
        {
            string[] lineArray = readLine.Split(' ');
            int wins = int.Parse(lineArray[1]);
            var returnTuple = new Tuple<string, int>(lineArray[0], wins);
            return returnTuple;
        }
    }
}
