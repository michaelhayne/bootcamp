﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;

namespace BattleShip.UI
{
    class Player
    {
        public string name { get; set; }
        public Board board { get; set; }

        public void SetValidName()
        {
            string inputString = null;
            bool validName = false;
            while (!validName)
            {
                inputString = Console.ReadLine();
                if (!inputString.Contains(" "))
                {
                    validName = true;
                }
                else
                {
                    Console.Write("\nName cannot contain spaces: ");
                }
            }
            name = inputString;
        }
    }
}
