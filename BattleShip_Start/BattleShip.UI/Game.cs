﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Game
    {
        public static void PlayGame()
        {
            bool newGame = false;
            do
            {
                Console.WriteLine(
                    @" ___________________________
|                           |
|   WELCOME TO BATTLESHIP   |
|___________________________|"
                    );
                // Set up 2 players and prompt users for names
                Player player1 = new Player();
                Player player2 = new Player();
                Console.Write("\nEnter PLAYER-1 name: ");
                player1.SetValidName();
                Console.Write("\nEnter PLAYER-2 name: ");
                player2.SetValidName();

                // Assign a board to each player
                Board board1 = new Board();
                Board board2 = new Board();
                player1.board = board1;
                player2.board = board2;

                Console.Clear();
                Console.WriteLine(player1.name + " VS. " + player2.name);
                Console.ReadLine();

                // Have each player place ships on their board
                Console.Clear();
                InitializeBoard.PlaceShips(player1);
                Console.Clear();
                InitializeBoard.PlaceShips(player2);

                // Loop through the gameplay phase until the game is finished
                bool gameFinished = false;
                Player winner = null;
                while (!gameFinished)
                {
                    Console.Clear();
                    Console.WriteLine("{0} take your turn\n", player1.name);
                    bool turnVictory = Turn.TakeTurn(player1, player2.board);
                    if (turnVictory)
                    {
                        winner = player1;
                        gameFinished = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("{0} take your turn\n", player2.name);
                        turnVictory = Turn.TakeTurn(player2, player1.board);
                        if (turnVictory)
                        {
                            winner = player2;
                            gameFinished = true;
                        }
                    }
                }
                Console.WriteLine("Congratulations {0}, you win!", winner.name);
                HighScore.AddScore(winner.name);
                Console.ReadLine();

                // Ask user if they would like to start a new game and assign true or false to 'newGame' accordingly
                bool validInput = false;
                while (!validInput)
                {
                    Console.Write("Play again? (Y or N): ");
                    string response = Console.ReadLine().ToUpper();
                    Console.Clear();
                    if (response == "Y")
                    {
                        newGame = true;
                        validInput = true;
                    }
                    else if (response == "N")
                    {
                        newGame = false;
                        validInput = true;
                    }
                    else
                    {
                        Console.WriteLine("Enter either Y(yes) or N(no)");
                    }
                }
            } while (newGame);
        }
    }
}
