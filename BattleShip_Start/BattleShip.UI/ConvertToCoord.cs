﻿using BattleShip.BLL.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class ConvertToCoord
    {
        // Convert a string into a Coordinate object
        public static Coordinate toCoord (string inputString)
        {
            if (isValidInput(inputString))
            {
                int x = ExtractX(inputString);
                int y = ExtractY(inputString);
                Coordinate coord = new Coordinate(x, y);
                return coord;
            }
            else
            {
                return null;
            }
        }

        // Check the string to make sure it is in a valid coordinate format 
        public static bool isValidInput (string inputString)
        {
            if ((inputString.Length < 2) || (inputString.Length > 3))
            {
                return false;
            }
            char xChar = inputString[0];
            int xInt = ConvertToInt(xChar);
            string yString = inputString.Substring(1);
            int yInt = 0;
            Int32.TryParse(yString, out yInt);
            if ((xInt == 0) || (yInt == 0) || (yInt > 10))
            {
                return false;
            }
            return true;

        }

        // Convert the x-coordinate of the string into an int
        public static int ExtractX (string inputString)
        {
            char xChar = inputString[0];
            int xInt = ConvertToInt(xChar);
            return xInt;
        }

        // Convert the y-coordinate of the string into an int
        public static int ExtractY (string inputString)
        {
            string yString = inputString.Substring(1);
            int yInt = 0;
            Int32.TryParse(yString, out yInt);
            return yInt;
        }

        // Convert a letter character A-J into the corresponding int value
        public static int ConvertToInt(char letter)
        {
            char upperLetter = char.ToUpper(letter);
            switch (upperLetter)
            {
                case 'A': return 1;
                case 'B': return 2;
                case 'C': return 3;
                case 'D': return 4;
                case 'E': return 5;
                case 'F': return 6;
                case 'G': return 7;
                case 'H': return 8;
                case 'I': return 9;
                case 'J': return 10;
                default: return 0;
            }
        }
    }
}
