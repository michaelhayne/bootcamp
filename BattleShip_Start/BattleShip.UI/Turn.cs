﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Turn
    {
        // Method for a player to take a turn.
        // Takes the board of the opposite player as the second parameter.
        // Returns a boolean true if the turn results in a victory, otherwise it returns false.
        public static bool TakeTurn(Player player, Board otherBoard)
        {
            DisplayBoard(otherBoard);
            Console.Write("\nSelect coordinate to fire on: ");
            FireShotResponse shotResult = ShotPrompt(otherBoard);
            Console.ReadLine();
            if (shotResult.ShotStatus == ShotStatus.Victory)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Prompt user for a coordinate to fire on.
        // If the coordinate provided is invalid then loop back and prompt for a new input.
        // Return a FireShotResponse object which contains info about the result of the shot.
        public static FireShotResponse ShotPrompt(Board board)
        {
            FireShotResponse response = new FireShotResponse();
            bool validShot = false;
            while (!validShot)
            {
                bool goodInput = false;
                while (!goodInput)
                {
                    String coordString = Console.ReadLine();
                    Coordinate coord = ConvertToCoord.toCoord(coordString);
                    if (coord != null)
                    {
                        response = board.FireShot(coord);
                        switch (response.ShotStatus)
                        {
                            case ShotStatus.Invalid:
                                Console.WriteLine("Invalid coordinate, try again");
                                break;
                            case ShotStatus.Duplicate:
                                Console.WriteLine("Already tried that coordinate, try again");
                                break;
                            case ShotStatus.Miss:
                                Console.WriteLine("MISS FOOL");
                                validShot = true;
                                break;
                            case ShotStatus.Hit:
                                Console.WriteLine("You hit the --{0}--", response.ShipImpacted);
                                validShot = true;
                                break;
                            case ShotStatus.HitAndSunk:
                                Console.WriteLine("You sunk the --{0}--", response.ShipImpacted);
                                validShot = true;
                                break;
                            case ShotStatus.Victory:
                                Console.WriteLine("YOU WON");
                                validShot = true;
                                break;
                        }
                        goodInput = true;
                    }
                    else
                    {
                        Console.WriteLine("Rethink your coordinates there partner");
                    }
                }
            }
            return response;
        }

        // Display the board that is being fired on with previous hits and misses marked
        public static void DisplayBoard(Board board)
        {
            Console.WriteLine("    A  B  C  D  E  F  G  H  I  J\n");
            // Each loop of 'i' fills a row of the board
            for (int i = 1; i < 11; i++)
            {
                if (i == 10)
                {
                    Console.Write(i);
                }
                else
                {
                    Console.Write(" {0}", i);
                }
                // Each loop of 'j' handles an individual point in the current row, evaluated left to right visually
                for (int j = 1; j < 11; j++)
                {
                    Coordinate coord = new Coordinate(j, i);
                    if (board.ShotHistory.ContainsKey(coord))
                    {
                        ShotHistory shotHistory = board.ShotHistory[coord];
                        char letter = ' ';
                        switch (shotHistory)
                        {
                            case ShotHistory.Hit:
                                letter = 'H';
                                Console.ForegroundColor = ConsoleColor.Red;
                                break;
                            case ShotHistory.Miss:
                                letter = 'M';
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            case ShotHistory.Unknown:
                                letter = 'U';
                                break;
                            default:
                                letter = 'E';
                                break;
                        }
                        Console.Write("  {0}", letter);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write("   ");
                    }
                }
                Console.WriteLine('\n');
            }
        }
    }
}
