﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class BattleShipGame
    {
        Board gameBoard = new Board();

        public void StartGame()
        {
            Console.WriteLine(
                @" ___________________________
|                           |
|   WELCOME TO BATTLESHIP   |
|___________________________|"
                );

            Player player1 = new Player();
            Player player2 = new Player();
            Console.Write("\nEnter PLAYER-1 name: ");
            player1.name = Console.ReadLine();
            Console.Write("\nEnter PLAYER-2 name: ");
            player2.name = Console.ReadLine();
            Console.Clear();
            Console.WriteLine(player1.name + " VS. " + player2.name);
            Console.ReadLine();

            InitializeBoard(gameBoard);

            Coordinate coord2 = new Coordinate(1, 2);
            Coordinate coord5 = new Coordinate(5, 5);

            //gameBoard.ShotHistory.Add(coord2, BLL.Responses.ShotHistory.Unknown);
            Console.WriteLine(gameBoard.ShotHistory.ContainsKey(coord2));
            Console.ReadLine();

            SetGameBoard(gameBoard);
            Console.WriteLine(gameBoard.ShotHistory.ContainsKey(coord5));
            Console.ReadLine();
            DisplayBoard(gameBoard);
        }

        public void SetGameBoard(Board x)
        {
            Coordinate coord3 = new Coordinate(5, 5);
            //x.ShotHistory.Add(coord3, BLL.Responses.ShotHistory.Unknown);
        }

        public void InitializeBoard(Board gameBoard)
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    Coordinate coord = new Coordinate(i, j);
                    gameBoard.ShotHistory.Add(coord, BLL.Responses.ShotHistory.Unknown);
                }
            }
        }




        public void DisplayBoard(Board gameBoard)
        {
            Console.WriteLine("\tA\tB\tC\tD\tE\tF\tG\tH\tI\tJ\n");
            for (int i = 1; i < 11; i++)
            {
                Console.Write(i);
                for (int j = 1; j < 11; j++)
                {
                    Coordinate coord = new Coordinate(i, j);
                    if (gameBoard.ShotHistory.ContainsKey(coord))
                    {
                        Console.Write("%%");
                    }
                    else
                    {
                        Console.Write("\t{0}{1}", ConvertToLet(i), j);
                    }
                }
                Console.WriteLine('\n');
            }
            Console.ReadLine();
        }





        public char ConvertToLet(int number)
        {
            switch(number)
            {
                case 1: return 'A';
                case 2: return 'B';
                case 3: return 'C';
                case 4: return 'D';
                case 5: return 'E';
                case 6: return 'F';
                case 7: return 'G';
                case 8: return 'H';
                case 9: return 'I';
                case 10: return 'J';
                default:
                    Console.WriteLine("Invalid X-coordinate number.");
                    return ' ';
            }       
        }

        public int ConvertToNum(char letter)
        {
            switch(letter)
            {
                case 'A': return 1;
                case 'B': return 2;
                case 'C': return 3;
                case 'D': return 4;
                case 'E': return 5;
                case 'F': return 6;
                case 'G': return 7;
                case 'H': return 8;
                case 'I': return 9;
                case 'J': return 10;
                default:
                    Console.WriteLine("Invalid X-coordinate letter.");
                    return 0;
            }
        }
    }
}
