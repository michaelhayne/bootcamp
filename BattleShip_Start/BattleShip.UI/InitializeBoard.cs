﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Ships;
using BattleShip.BLL.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class InitializeBoard
    {
        // Prompt player to place a ship for each ship type
        public static void PlaceShips(Player player)
        {
            Board board = player.board;

            Console.WriteLine("{0} place your ships\n", player.name);
            DisplayShips(board);
            PlaceShipPrompt(board, ShipType.Battleship);
            Console.Clear();

            Console.WriteLine("{0} place your ships\n", player.name);
            DisplayShips(board);
            PlaceShipPrompt(board, ShipType.Carrier);
            Console.Clear();

            Console.WriteLine("{0} place your ships\n", player.name);
            DisplayShips(board);
            PlaceShipPrompt(board, ShipType.Cruiser);
            Console.Clear();

            Console.WriteLine("{0} place your ships\n", player.name);
            DisplayShips(board);
            PlaceShipPrompt(board, ShipType.Destroyer);
            Console.Clear();

            Console.WriteLine("{0} place your ships\n", player.name);
            DisplayShips(board);
            PlaceShipPrompt(board, ShipType.Submarine);
            Console.Clear();

            Console.WriteLine("{0} all ships placed\n", player.name);
            DisplayShips(board);
            Console.Write("\nPress ENTER to continue: ");
            Console.ReadLine();
            Console.Clear();
        }

        // Place a ship of type 'shipType' on the player's board
        public static void PlaceShipPrompt(Board board, ShipType shipType)
        {
            // Build a PlaceShipRequest to send to the PlaceShip method
            PlaceShipRequest shipReq = new PlaceShipRequest();
            shipReq.ShipType = shipType;

            bool placeShipOK = false;
            while (!placeShipOK)
            {
                // Prompt user for a coordinate string and check if that string can be converted into a Coordinate object
                bool goodInput = false;
                Console.Write("\n--{0}-- starting coordinate: ", shipType);
                while (!goodInput)
                {
                    string inputString = Console.ReadLine();
                    Coordinate tempCoord = ConvertToCoord.toCoord(inputString);
                    if (tempCoord != null)
                    {
                        shipReq.Coordinate = tempCoord;
                        goodInput = true;
                    }
                    else
                    {
                        Console.WriteLine("Try again fool: ");
                    }
                }

                // Prompt user for a direction that the ship will face and check if the direction is valid
                bool directionInputValid = false;
                Console.Write("--{0}-- direction facing: ", shipType);
                while (!directionInputValid)
                {
                    string inputDirection = Console.ReadLine().ToUpper();
                    switch (inputDirection)
                    {
                        case "UP":
                            shipReq.Direction = ShipDirection.Up;
                            directionInputValid = true;
                            break;
                        case "DOWN":
                            shipReq.Direction = ShipDirection.Down;
                            directionInputValid = true;
                            break;
                        case "LEFT":
                            shipReq.Direction = ShipDirection.Left;
                            directionInputValid = true;
                            break;
                        case "RIGHT":
                            shipReq.Direction = ShipDirection.Right;
                            directionInputValid = true;
                            break;
                    }
                    if (!directionInputValid)
                    {
                        Console.WriteLine("Enter a valid direction (Up, Down, Left or Right): ");
                    }
                }

                // Send the PlaceShipRequest to Board.PlaceShip and check if the ship position will fit on the board
                ShipPlacement shipPlacement = board.PlaceShip(shipReq);
                if (shipPlacement == ShipPlacement.Ok)
                {
                    Console.WriteLine("--{0}-- placed successfully", shipType);
                    placeShipOK = true;
                }
                else if (shipPlacement == ShipPlacement.NotEnoughSpace)
                {
                    Console.WriteLine("Not enough space to place --{0}--, enter coordinates and direction again", shipType);
                }
                else if (shipPlacement == ShipPlacement.Overlap)
                {
                    Console.WriteLine("Placement of {0} overlaps with another ship, try again", shipType);
                }
            }
        }

        // Display the board with the positions of ships marked
        public static void DisplayShips(Board board)
        {
            // Store the coordinates used by ships in a list
            List<Coordinate> ShipPlacements = new List<Coordinate>();
            Ship[] ships = board.ViewShips();
            foreach (Ship ship in ships)
            {
                if (ship != null)
                {
                    Coordinate[] coords = ship.BoardPositions;
                    foreach (var coord in coords)
                    {
                        ShipPlacements.Add(coord);
                    }
                }
            }

            // Write the board with the ship positions to the console.
            // For each possible coordinate, check if that coordinate exists in the ShipPlacements list and mark it on the board if it does.
            Console.WriteLine("    A  B  C  D  E  F  G  H  I  J\n");
            for (int i = 1; i < 11; i++)
            {
                if (i == 10)
                {
                    Console.Write(i);
                }
                else
                {
                    Console.Write(" {0}", i);
                }
                for (int j = 1; j < 11; j++)
                {
                    Coordinate coord = new Coordinate(j, i);
                    if (ShipPlacements.Contains(coord))
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write("  S");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write("   ");
                    }
                }
                Console.WriteLine('\n');
            }
        }
    }
}
