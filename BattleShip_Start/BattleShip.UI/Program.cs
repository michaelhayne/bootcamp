﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            // Main menu section, prompts user to choose view highscores, start new game or quit program
            bool quitApplication = false;
            while (!quitApplication)
            {
                bool validSelection = false;
                string input = null;
                Console.WriteLine("H - view highscores\nN - start new game\nQ - exit application\n");
                while (!validSelection)
                {
                    input = Console.ReadLine().ToUpper();
                    if (input == "H" || input == "N" || input == "Q")
                    {
                        validSelection = true;
                    }
                    else
                    {
                        Console.Write("\nInvalid input\nPlease enter H, N or Q: ");
                    }
                }
                if (input == "H")
                {
                    Console.Clear();
                    HighScore.PrintHighScores();
                    Console.WriteLine(@" ______________
|              |
|  HIGHSCORES  |
|______________|        
");
                }
                else if (input == "N")
                {
                    Console.Clear();
                    Game.PlayGame();
                }
                else if (input == "Q")
                {
                    quitApplication = true;
                }
                Console.Clear();
            }
        }
    }
}
