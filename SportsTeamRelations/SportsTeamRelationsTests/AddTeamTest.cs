﻿using NUnit.Framework;
using SportsTeamRelations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsTeamRelationsTests
{
    [TestFixture]
    public class SportsTeamTests
    {
        [Test]
        public void SelectTeamNameTest()
        {
            using (var context = new SportsContext())
            {
                string expected = "Globetrotters";
                Team t = context.Teams.SingleOrDefault(x => x.TeamName == "Globetrotters");
                var actual = t.TeamName;
                Assert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void SelectTeamVenueTest()
        {
            using (var context = new SportsContext())
            {
                var team = context.Teams.SingleOrDefault(x => x.TeamName == "Globetrotters");
                var teamVenue = context.Venues.SingleOrDefault(x => x.Building == "The Yard");
                var expected = true;
                var actual = false;
                if (team.VenueID == teamVenue.VenueID)
                {
                    actual = true;
                }
                Assert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void SelectTeamYears()
        {
            using (var context = new SportsContext())
            {
                var expected = 1786;
                var team = context.Teams.SingleOrDefault(x => x.TeamName == "Globetrotters");
                var years = context.Years.SingleOrDefault(x => x.YearsID == team.YearsID);
                var actual = years.YearDisbanded;
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
