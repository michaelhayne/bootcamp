﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsTeamRelations
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new SportsContext())
            {
                //var team = new Team()
                //{
                //    TeamName = "Globetrotters",
                //    Sport = "Basketball",
                //    Coach = "Stretch",
                //    AvgSalary = 15.00M,
                //    Active = true,
                //};

                //var venue = new Venue()
                //{
                //    Building = "The Yard",
                //    City = "Rust City"
                //};

                //var years = new Years()
                //{
                //    YearFounded = 1785,
                //    YearDisbanded = 1786
                //};

                //context.Teams.Add(team);
                //context.Venues.Add(venue);
                //context.Years.Add(years);
                //context.SaveChanges();

                var query = from t in context.Teams select t;

                foreach (var x in query)
                {
                    Console.Write(x.TeamName + " --- ");
                }

                Console.WriteLine();

                Console.Write("Input a team name: ");
                string inputName = Console.ReadLine();

                var returnTeam = context.Teams.FirstOrDefault(x => x.TeamName == inputName);

                Console.WriteLine(returnTeam.TeamName + "\n" + returnTeam.Venue.Building + "\n" + returnTeam.Sport);

                Console.ReadLine();
            }
        }
    }
}
