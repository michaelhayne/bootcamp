﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsTeamRelations
{
    public class Team
    {
        public int TeamID { get; set; }
        public string TeamName { get; set; }
        public string Sport { get; set; }
        public int YearsID { get; set; }
        public int VenueID { get; set; }
        public string Coach { get; set; }
        public decimal AvgSalary { get; set; }
        public bool Active { get; set; }

        public virtual Venue Venue { get; set; }
        public virtual Years Years { get; set; }
    }
}
