﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsTeamRelations
{
    public class Years
    {
        public int YearsID { get; set; }
        public int YearFounded { get; set; }
        public int YearDisbanded { get; set; }
    }
}
