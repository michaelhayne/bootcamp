namespace SportsTeamRelations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Teams", "Url", x => x.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Teams", "Url");
        }
    }
}
