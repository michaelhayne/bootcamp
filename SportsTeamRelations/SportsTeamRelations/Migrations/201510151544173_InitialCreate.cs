namespace SportsTeamRelations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        TeamID = c.Int(nullable: false, identity: true),
                        TeamName = c.String(),
                        Sport = c.String(),
                        YearsID = c.Int(nullable: false),
                        VenueID = c.Int(nullable: false),
                        Coach = c.String(),
                        AvgSalary = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TeamID)
                .ForeignKey("dbo.Venues", t => t.VenueID, cascadeDelete: true)
                .ForeignKey("dbo.Years", t => t.YearsID, cascadeDelete: true)
                .Index(t => t.YearsID)
                .Index(t => t.VenueID);
            
            CreateTable(
                "dbo.Venues",
                c => new
                    {
                        VenueID = c.Int(nullable: false, identity: true),
                        Building = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.VenueID);
            
            CreateTable(
                "dbo.Years",
                c => new
                    {
                        YearsID = c.Int(nullable: false, identity: true),
                        YearFounded = c.Int(nullable: false),
                        YearDisbanded = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.YearsID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teams", "YearsID", "dbo.Years");
            DropForeignKey("dbo.Teams", "VenueID", "dbo.Venues");
            DropIndex("dbo.Teams", new[] { "VenueID" });
            DropIndex("dbo.Teams", new[] { "YearsID" });
            DropTable("dbo.Years");
            DropTable("dbo.Venues");
            DropTable("dbo.Teams");
        }
    }
}
