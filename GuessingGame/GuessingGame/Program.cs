﻿using System;

namespace GuessingGame
{
    class Program
    {
        static void Main()
        {
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;

            Random r = new Random(); // This is System's built in random number generator
            theAnswer = r.Next(1, 21); // Get a random from 1 to 20
            int[] answerArray = new int[20];

            do
            {
                // get player input
                Console.Write("Enter your guess: ");
                playerInput = Console.ReadLine();

                // attempt to convert to number
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (answerArray[playerGuess] == 0)
                    {
                        // see if they won
                        if (playerGuess == theAnswer)
                        {
                            Console.WriteLine("You got it!");
                            isNumberGuessed = true;
                        }
                        else
                        {
                            // they didn't win, so were they too high or too low?
                            if (playerGuess > theAnswer)
                                Console.WriteLine("Too high!");
                            else
                                Console.WriteLine("Too low!");
                        }
                        answerArray[playerGuess]++;
                    }
                    else
                    {
                        Console.WriteLine("You already guessed that");
                    }
                }
                else
                {
                    // learn to play n00b
                    Console.WriteLine("That wasn't a valid number!");
                }
            } while (!isNumberGuessed);

            int numOfGuesses = 0;
            Console.WriteLine("\nYou guessed:");
            for (int i = 1; i < answerArray.Length; i++)
            {
                if (0 < answerArray[i])
                {
                    numOfGuesses++;
                    Console.WriteLine(i);
                }
            }
            Console.WriteLine("\nThat's {0} guesses fool!", numOfGuesses);
            Console.Write("\nPress enter to quit.");
            Console.ReadLine();
        }
    }
}
