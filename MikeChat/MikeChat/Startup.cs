﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MikeChat.Startup))]
namespace MikeChat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
