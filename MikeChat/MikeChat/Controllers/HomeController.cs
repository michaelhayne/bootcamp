﻿using Microsoft.AspNet.Identity;
using MikeChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MikeChat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchUsers()
        {
            var allUsers = new List<ApplicationUser>();
            var allFriends = new List<Friend>();
            using (var context = new ApplicationDbContext())
            {
                allUsers = context.Users.ToList();
                allFriends = context.Friends.ToList();
            }

            ViewBag.AllUsers = allUsers;
            ViewBag.AllFriends = allFriends;

            return View();
        }

        [HttpPost]
        public ActionResult AddFriend(Friend newFriendship)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Friends.Add(newFriendship);
                context.SaveChanges();
            }
            return RedirectToAction("SearchUsers");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}