﻿using MikeChat.Extensions;
using MikeChat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MikeChat.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index(string userID)
        {
            var userProfile = new ApplicationUser();
            var userFriendsList = new List<ApplicationUser>();
            using (var context = new ApplicationDbContext())
            {
                userProfile = context.Users.SingleOrDefault(x => x.Id == userID);
            }

            //var profilePicturePath = Directory.GetFiles(Server.MapPath("~/Uploads"), userID);
            //var pictureFileInfo = new DirectoryInfo(Server.MapPath("~/Uploads")).EnumerateFiles(userID + "*").FirstOrDefault();
            //string pictureString = null;
            //if (pictureFileInfo != null)
            //{
            //    pictureString = pictureFileInfo.ToString();
            //}
            //else
            //{
            //    pictureString = "default.png";
            //}
            string pictureString = PictureExtensions.GetProfilePicture(userID);
            ViewBag.ProfilePicture = pictureString;

            var friendList = FriendExtensions.GetUserFriends(userID);
            bool moreThanNineFriends = false;
            if (friendList.Count > 9)
            {
                moreThanNineFriends = true;
            }
            ViewBag.FriendsList = FriendExtensions.SelectTenRandom(FriendExtensions.GetUserFriends(userID));
            ViewBag.MoreThanNineFriends = moreThanNineFriends;

            ViewBag.PostList = PostExtensions.GetWallPosts(userID);
            ViewBag.CommentList = PostExtensions.GetWallComments(userID);

            return View(userProfile);
        }

        [HttpPost]
        public ActionResult SubmitWallPost(WallPost wallPost)
        {
            wallPost.PostDate = DateTime.Now;
            if ((wallPost.TextContent != null) || (wallPost.ImageContent != null))
            {
                using (var context = new ApplicationDbContext())
                {
                    context.WallPosts.Add(wallPost);
                    context.SaveChanges();
                }
            }
            return RedirectToAction("Index", new { userID = wallPost.WallID });
        }

        [HttpPost]
        public ActionResult SubmitComment(string wallID, Comment comment)
        {
            comment.CommentDate = DateTime.Now;
            if (comment.TextContent != null)
            {
                using (var context = new ApplicationDbContext())
                {
                    context.Comments.Add(comment);
                    context.SaveChanges();
                }
            }
            return RedirectToAction("Index", new { userID = wallID });
        }

        [HttpPost]
        public ActionResult UploadProfilePicture(ProfilePicture profilePicture)
        {
            //var convertedProfilePicture = new ConvertedPicture()
            //{
            //    UserID = profilePicture.UserID
            //};

            

            //using (var context = new ApplicationDbContext())
            //{
            //    context.ProfilePictures.Add(convertedProfilePicture);
            //    context.SaveChanges();
            //}

            if ((profilePicture.UserPicture != null) && (profilePicture.UserPicture.ContentLength > 0))
            {
                try
                {
                    var croppedImage = PictureExtensions.CropToSquare(profilePicture.UserPicture);
                    //string path = "~/Uploads/" + profilePicture.UserID + "." + profilePicture.UserPicture.ContentType.Substring(6);
                    string path = Server.MapPath("~/Uploads") + "\\" + profilePicture.UserID + "." + profilePicture.UserPicture.ContentType.Substring(6);
                    //string path = Path.Combine(Server.MapPath("~/Uploads"), Path.GetFileName(profilePicture.UserPicture.FileName));
                    PictureExtensions.DeletePictureByID(profilePicture.UserID);
                    croppedImage.Save(path);
                    //profilePicture.UserPicture.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR: " + ex.Message;
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file";
            }

            return RedirectToAction("Index", new { userID = profilePicture.UserID });
        }

        //public ActionResult UserProfile(string userID)
        //{
        //    var userProfile = new ApplicationUser();
        //    using (var context = new ApplicationDbContext())
        //    {
        //        userProfile = (ApplicationUser)context.Users.Where(x => x.Id == userID);
        //    }
        //    return View(userProfile);
        //}
    }
}