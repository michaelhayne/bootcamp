﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System;

namespace MikeChat.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }

        //public virtual ProfilePicture ProfilePicture { get; set; }
        public virtual ICollection<Friend> Friends { get; set; }
        public virtual ICollection<WallPost> WallPosts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("DisplayName", this.DisplayName));

            return userIdentity;
        }
    }

    public class WallPost
    {
        [Key]
        public int PostID { get; set; }

        public string WallID { get; set; }
        public string PosterID { get; set; }

        public DateTime PostDate { get; set; }
        public string TextContent { get; set; }
        public byte[] ImageContent { get; set; }



        //public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Comment
    {
        [Key]
        public int CommentID { get; set; }
        public int PostID { get; set; }
        public string CommenterID { get; set; }
        public DateTime CommentDate { get; set; }
        public string TextContent { get; set; }
    }

    public class ProfilePicture
    {
        //[Key]
        //[ForeignKey("ApplicationUser")]
        public string UserID { get; set; }

        public HttpPostedFileBase UserPicture { get; set; }

        //public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Friend
    {
        [Key, Column(Order = 0)]
        public string UserID_1 { get; set; }

        [Key, Column(Order = 1)]
        public string UserID_2 { get; set; }

        public string ActionUserID { get; set; }
        public int Status { get; set; }

        public virtual ApplicationUser User_1 { get; set; }
        public virtual ApplicationUser User_2 { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("MikeChatConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //public DbSet<ProfilePicture> ProfilePictures { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<WallPost> WallPosts { get; set; }
        public DbSet<Friend> Friends { get; set; }
    }
}