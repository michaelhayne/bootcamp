﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MikeChat.Models
{
    public class ConvertedPicture
    {
        public string UserID { get; set; }
        public byte[] UserPicture { get; set; }
    }
}