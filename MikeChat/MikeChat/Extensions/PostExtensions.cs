﻿using MikeChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MikeChat.Extensions
{
    public static class PostExtensions
    {
        public static List<WallPost> GetWallPosts(string userID)
        {
            var wallPostList = new List<WallPost>();
            using (var context = new ApplicationDbContext())
            {
                wallPostList = context.WallPosts.Where(x => x.WallID == userID).OrderByDescending(x => x.PostDate).ToList();
            }
            return wallPostList;
        }

        public static List<Comment> GetWallComments(string wallID)
        {
            var commentList = new List<Comment>();
            using (var context = new ApplicationDbContext())
            {
                var commentQuery = from a in context.WallPosts
                                   join b in context.Comments
                                   on a.PostID equals b.PostID
                                   where a.WallID == wallID
                                   select b;
                commentList = commentQuery.ToList();
            }
            return commentList;
        }
    }
}