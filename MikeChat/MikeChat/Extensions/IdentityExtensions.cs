﻿using MikeChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace MikeChat.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetDisplayName(this IIdentity identity)
        {
            return ((ClaimsIdentity)identity).FindFirst("DisplayName").Value;
        }

        public static string GetDisplayNameByID(string userID)
        {
            string displayName = null;
            using (var context = new ApplicationDbContext())
            {
                displayName = context.Users.Find(userID).DisplayName;
            }
            return displayName;
        }

        public static void DeleteUserByID(string userID)
        {
            using (var context = new ApplicationDbContext())
            {
                var userRow = context.Users.Find(userID);
                var friendRows = context.Friends.Where(x => (x.UserID_1 == userID) || (x.UserID_2 == userID));
                var postRows = context.WallPosts.Where(x => (x.WallID == userID) || (x.PosterID == userID));
                var commentRows = context.Comments.Where(x => x.CommenterID == userID);

                context.Users.Remove(userRow);
                context.Friends.RemoveRange(friendRows);
                context.WallPosts.RemoveRange(postRows);
                context.Comments.RemoveRange(commentRows);

                var postRowIDs = postRows.Select(x => x.PostID);
                var moreCommentRows = context.Comments.Where(x => postRowIDs.Contains(x.PostID));
                context.Comments.RemoveRange(moreCommentRows);

                context.SaveChanges();
            }

            PictureExtensions.DeletePictureByID(userID);
        }
    }
}