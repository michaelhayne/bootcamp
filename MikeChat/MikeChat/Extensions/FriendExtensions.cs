﻿using MikeChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MikeChat.Extensions
{
    public static class FriendExtensions
    {
        public static bool ContainsFriendPair(List<Friend> allFriends, string id_1, string id_2)
        {
            bool alreadyFriends = allFriends.Any(x => ((x.UserID_1 == id_1) && (x.UserID_2 == id_2)) || ((x.UserID_1 == id_2) && (x.UserID_2 == id_1)));
            return alreadyFriends;
        }

        public static List<ApplicationUser> GetUserFriends(string userID) 
        {
            var userFriendsList = new List<ApplicationUser>();
            using (var context = new ApplicationDbContext())
            {
                var friendPairsList = context.Friends.Where(x => (x.UserID_1 == userID) || (x.UserID_2 == userID)).ToList();
                foreach (var friendPair in friendPairsList)
                {
                    if (friendPair.UserID_1 == userID)
                    {
                        var friend = context.Users.SingleOrDefault(x => x.Id == friendPair.UserID_2);
                        userFriendsList.Add(friend);
                    }
                    else if (friendPair.UserID_2 == userID)
                    {
                        var friend = context.Users.SingleOrDefault(x => x.Id == friendPair.UserID_1);
                        userFriendsList.Add(friend);
                    }
                }
            }
            return userFriendsList;
        }

        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static List<ApplicationUser> SelectTenRandom(List<ApplicationUser> friendList)
        {
            var returnList = new List<ApplicationUser>();
            friendList.Shuffle();
            if (friendList.Any())
            {
                for (var i = 0; i < 9; i++)
                {
                    if (friendList[i] != null)
                    {
                        returnList.Add(friendList[i]);
                        //THIS CAUSES NULL REFERENCE
                        if (i >= (friendList.Count - 1))
                        {
                            i = 9;
                        }
                    }
                }
            }
            return returnList;
        }
    }
}