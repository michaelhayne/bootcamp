﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Hosting;

namespace MikeChat.Extensions
{
    public static class PictureExtensions
    {
        public static string GetProfilePicture(string userID)
        {
            var pictureFileInfo = new DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath + "Uploads").EnumerateFiles(userID + "*").FirstOrDefault();
            string pictureString = null;
            if (pictureFileInfo != null)
            {
                pictureString = pictureFileInfo.ToString();
            }
            else
            {
                pictureString = "default.png";
            }
            return pictureString; 
        }

        public static void DeletePictureByID(string userID)
        {
            var pictureFileInfo = new DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath + "/Uploads").EnumerateFiles(userID + "*").FirstOrDefault();
            if (pictureFileInfo != null)
            {
                string picturePath = HostingEnvironment.ApplicationPhysicalPath + "Uploads\\" + pictureFileInfo.ToString();
                File.Delete(picturePath);
            }
        }

        public static WebImage CropToSquare(HttpPostedFileBase sourceImage)
        {
            var newImage = new WebImage(sourceImage.InputStream);

            var width = newImage.Width;
            var height = newImage.Height;

            if (width > height)
            {
                var leftRightCrop = (width - height) / 2;
                newImage.Crop(0, leftRightCrop, 0, leftRightCrop);
            }
            else if (height > width)
            {
                var topBottomCrop = (height - width) / 2;
                newImage.Crop(topBottomCrop, 0, topBottomCrop, 0);
            }

            return newImage;
        }
    }
}