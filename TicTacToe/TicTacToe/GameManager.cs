﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class GameManager
    {
        private bool _gameOver;

        private Player _player1;
        private Player _player2;
        private Player _currentPlayer;
        private Player _otherPlayer;
        private Board _board;

        public void PlayGame()
        {
            // just keep on setting up boards and playing the game until they indicate
            // that they'd like to quit the program
            bool newGame = false;
            do
            {
                _board = new Board();
                _gameOver = false;
                Console.Write(@" ___________________________
|                           |
|   WELCOME TO TIC-TAC-TOE  |
|___________________________|");
                Console.WriteLine("\n\n(To set computer player enter name as 'computer')\n");
                _player1 = CreatePlayer(1, "X");
                _player2 = CreatePlayer(2, "O");

                ProcessTurns();

                Console.Write("\nPlay again? (Y or N): ");
                bool validInput = false;
                while (!validInput)
                {
                    string yesNo = Console.ReadLine().ToUpper();
                    if (yesNo == "Y")
                    {
                        newGame = true;
                        validInput = true;
                    }
                    else if (yesNo == "N")
                    {
                        newGame = false;
                        validInput = true;
                    }
                    else
                    {
                        Console.Write("\nPlease type 'Y' or 'N': ");
                    }
                }
                Console.Clear();
            } while (newGame);
        }
        
        // This method loops through alternating player turns until one player wins.
        // If a player's name is 'computer' it uses the ComputerTurn method, otherwise it uses PromptUser to take the turn.
        private void ProcessTurns()
        {
            while (!_gameOver)
            {
                NextPlayer();
                _board.Display();
                if (_currentPlayer.Name == "computer")
                {
                    ComputerTurn(_currentPlayer, _otherPlayer);
                }
                else
                {
                    PromptUser(_currentPlayer);
                }
                _gameOver = _board.IsVictory(_currentPlayer) || _board.IsCatGame();
            }
            Console.ReadLine();
        }

        // This method contains the board placement logic that decides where a computer player will place a mark
        private void ComputerTurn(Player computer, Player otherPlayer)
        {
            Console.WriteLine("Computer ({0}'s) going, hit enter", computer.Mark);
            Console.ReadLine();
            int decision;
            bool alreadyPlaced = false;
            string[] boardArray = _board.GetBoardArray();
            int spaceNumber = 1;

            // This foreach loop checks the highest priority considerations in order of importance
            foreach (string space in boardArray)
            {
                if (space == spaceNumber.ToString())
                {
                    // Check if the space will result in the computer player's victory
                    _board.ChangeBoardArray(computer.Mark, spaceNumber);
                    if (_board.TempVictoryCheck(computer))
                    {
                        _board.ChangeBoardArray(spaceNumber.ToString(), spaceNumber);
                        decision = int.Parse(space);
                        _board.AddMark(computer.Mark, decision);
                        alreadyPlaced = true;
                        break;
                    }
                    else
                    {
                        _board.ChangeBoardArray(spaceNumber.ToString(), spaceNumber);
                    }

                    // Check if the space will result in the other player's victory
                    _board.ChangeBoardArray(otherPlayer.Mark, spaceNumber);
                    if (_board.TempVictoryCheck(otherPlayer))
                    {
                        _board.ChangeBoardArray(spaceNumber.ToString(), spaceNumber);
                        decision = int.Parse(space);
                        _board.AddMark(computer.Mark, decision);
                        alreadyPlaced = true;
                        break;
                    }
                    else
                    {
                        _board.ChangeBoardArray(spaceNumber.ToString(), spaceNumber);
                    }

                    // Check if the space is the middle space
                    if (space == "5")
                    {
                        decision = int.Parse(space);
                        _board.AddMark(computer.Mark, decision);
                        alreadyPlaced = true;
                        break;
                    }
                }
                spaceNumber++;
            }

            // If none of the top priority cases resulted in a placement, try to place the mark in a corner space
            if (!alreadyPlaced)
            {
                spaceNumber = 1;
                foreach (string space in boardArray)
                {
                    // All the corner placements are identified with an odd number so check if the current space is odd-numbered
                    if ((space == spaceNumber.ToString()) && (spaceNumber % 2 == 1))
                    {
                        decision = int.Parse(space);
                        _board.AddMark(computer.Mark, decision);
                        alreadyPlaced = true;
                        break;
                    }
                    spaceNumber++;
                }
            }

            // If none of the checks resulted in a placement, then place the mark in a random available space
            if (!alreadyPlaced)
            {
                Random rnd = new Random();
                bool validPlacement = false;
                while (!validPlacement)
                {
                    decision = rnd.Next(1, 10);
                    if (boardArray[decision - 1] == decision.ToString())
                    {
                        _board.AddMark(computer.Mark, decision);
                        validPlacement = true;
                    }
                }
            }
        }

        // Prompt the human player to take their turn
        private void PromptUser(Player player)
        {
            Console.Write("{0} ({1}'s) select a space: ", player.Name, player.Mark);
            bool validSelection = false;
            while (!validSelection)
            {
                // Check whether input is an integer, then check if that integer is in the board's range 1-9.
                // If it is valid input, AddMark returns true if the space is available or false if the space is already taken.
                string selection = Console.ReadLine();
                int selectionInt;
                bool isInt = int.TryParse(selection, out selectionInt);
                if (isInt)
                {
                    if (0 < selectionInt && selectionInt < 10)
                    {
                        validSelection = _board.AddMark(player.Mark, selectionInt);
                    }
                }
                Console.Write("\nInvalid selection, try again: ");
            }
        }

        private void SetUp()
        {
            // set up the game
        }

        // Alternates between players.
        // If a new game is started then order of player turns will switch, meaning 'O' will go first on the second play through.
        private void NextPlayer()
        {
            if (_currentPlayer == null || _currentPlayer.Number == 2)
            {
                _currentPlayer = _player1;
                _otherPlayer = _player2;
            }
            else
            {
                _currentPlayer = _player2;
                _otherPlayer = _player1;
            }
        }

        private Player CreatePlayer(int number, string mark)
        {
            // prompt and create a player
            Player player = new Player();
            player.Number = number;
            player.Mark = mark;
            Console.Write("Player {0} enter your name: ", number);
            player.Name = Console.ReadLine();
            return player;
        }
    }
}
