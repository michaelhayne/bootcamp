﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Board
    {
        private string[] _boardArray = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        private int _turnsCounter = 0;

        public bool AddMark(string mark, int position)
        {
            if (_boardArray[position-1] == position.ToString())
            {
                _boardArray[position-1] = mark;
                _turnsCounter += 1;
                return true;
            }

            Console.WriteLine("That position is filled!");
            return false;
        }

        // Checks if a space would cause a victory without actually declaring a victory
        public bool TempVictoryCheck(Player currentPlayer)
        {
            bool victory = IsHorizontalWin(currentPlayer.Mark) || IsVerticalWin(currentPlayer.Mark) || IsDiagonalWin(currentPlayer.Mark);
            return victory;
        }

        // Used in the GameManager class to temporarily change a mark on the board
        public void ChangeBoardArray(string mark, int position)
        {
            _boardArray[position - 1] = mark;
        }

        // Used to access the private board array in other classes 
        public string[] GetBoardArray()
        {
            return _boardArray;
        }

        // Wrote a new display method that uses a WriteSpace method to display marks with different colors
        public void Display()
        {
            /* Console.Clear();
            Console.WriteLine(" {0} | {1} | {2}", _boardArray[0], _boardArray[1], _boardArray[2]);
            Console.WriteLine("---------------");
            Console.WriteLine(" {0} | {1} | {2}", _boardArray[3], _boardArray[4], _boardArray[5]);
            Console.WriteLine("---------------");
            Console.WriteLine(" {0} | {1} | {2}\n\n", _boardArray[6], _boardArray[7], _boardArray[8]); */

            Console.Clear();
            Console.Write(" "); WriteSpace(_boardArray[0]); Console.Write(" | "); WriteSpace(_boardArray[1]); Console.Write(" | "); WriteSpace(_boardArray[2]);
            Console.WriteLine("\n-----------");
            Console.Write(" "); WriteSpace(_boardArray[3]); Console.Write(" | "); WriteSpace(_boardArray[4]); Console.Write(" | "); WriteSpace(_boardArray[5]);
            Console.WriteLine("\n-----------");
            Console.Write(" "); WriteSpace(_boardArray[6]); Console.Write(" | "); WriteSpace(_boardArray[7]); Console.Write(" | "); WriteSpace(_boardArray[8]);
            Console.WriteLine("\n");
        }

        // If the mark in the space is 'X' or 'O' this method will temporarily change the console foreground color and then reset it
        public void WriteSpace(string arraySpace)
        {
            if (arraySpace == "X")
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (arraySpace == "O")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            Console.Write(arraySpace);
            Console.ResetColor();
        }

        public bool IsVictory(Player currentPlayer)
        {
            bool victory = IsHorizontalWin(currentPlayer.Mark) || IsVerticalWin(currentPlayer.Mark) || IsDiagonalWin(currentPlayer.Mark);
            if (victory == true)
            {
                Display();
                Console.WriteLine("{0} wins!", currentPlayer.Name);               
            }

            return victory;
        }

        private bool IsHorizontalWin(string mark)
        {
            return (_boardArray[0] == mark && _boardArray[1] == mark && _boardArray[2] == mark) ||
                   (_boardArray[3] == mark && _boardArray[4] == mark && _boardArray[5] == mark) ||
                   (_boardArray[6] == mark && _boardArray[7] == mark && _boardArray[8] == mark);
        }

        private bool IsVerticalWin(string mark)
        {
            return (_boardArray[0] == mark && _boardArray[3] == mark && _boardArray[6] == mark) ||
                   (_boardArray[1] == mark && _boardArray[4] == mark && _boardArray[7] == mark) ||
                   (_boardArray[2] == mark && _boardArray[5] == mark && _boardArray[8] == mark);
        }

        private bool IsDiagonalWin(string mark)
        {
            return (_boardArray[0] == mark && _boardArray[4] == mark && _boardArray[8] == mark) ||
                   (_boardArray[2] == mark && _boardArray[4] == mark && _boardArray[6] == mark);
        }

        public bool IsCatGame()
        {
            Display();
            Console.WriteLine("It's a tie!");
            return _turnsCounter == 9;
        }
    }
}
