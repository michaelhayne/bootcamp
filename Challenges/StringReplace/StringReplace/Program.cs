﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringReplace
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "She sells sea shells on the sea shore";
            string target = "s";
            string replacements = "$$";
            StringReplace(sentence, target, replacements);
            Console.ReadLine();
        }

        public static void StringReplace(string sentence, string targetLetters, string replacement)
        {
            char[] sentenceArray = sentence.ToCharArray(); 
            for (var i = 0; i < sentence.Length; i++)
            {
                char[] comparison = new char[targetLetters.Length];
                if ((sentence[i] == targetLetters[0]))
                {
                    bool match = true;
                    int index = 0;
                    while (match)
                    {
                        if ((replacement.Length < index + 1) || (sentence.Length < i + index + 1))
                        {
                            match = false;
                        }
                        else if (sentence[i + index] == targetLetters[index])
                        {
                            comparison[index] = targetLetters[index];
                            index++;
                        }
                        else
                        {
                            match = false;
                        }
                    }

                    bool comparisonMatch = true;
                    for (var k = 0; k < targetLetters.Length; k++)
                    {
                        if (comparison[k] != targetLetters[k])
                        {
                            comparisonMatch = false;
                        }
                    }

                    if (comparisonMatch)
                    {
                        for (var l = 0; l < targetLetters.Length; l++)
                        {
                            sentenceArray[i + l] = replacement[l];
                        }
                    }
                }
            }
            string sentenceReplaced = new string(sentenceArray);
            Console.WriteLine(sentenceReplaced);
        }
    }
}
