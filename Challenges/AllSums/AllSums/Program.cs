﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllSums
{
    class Program
    {
        static void Main()
        {
            var masterList = new List<List<int>>();
            var nums = new int[] { 2, 4, 1, 6, 3, 7, 5 };
            RunAllSums(nums, 7);
            Console.ReadLine();
        }

        public static void RunAllSums(int[] theArray, int theNumber)
        {
            Array.Sort(theArray);
            var numList = new List<int>();
            AllSums(theArray, theNumber, numList);
        }

        public static void AllSums(int[] numArray, int targetNum, List<int> numList)
        {
            var tempList = new List<int>();
            int currentSum = 0;

            foreach (int num in numList)
            {
                tempList.Add(num);
            }

            if (tempList.Any())
            {
                currentSum = tempList.Sum();
            }

            if (!(numArray.Length == 0))
            {
                if (currentSum + numArray[0] == targetNum)
                {
                    tempList.Add(numArray[0]);
                    foreach (int num in tempList)
                    {
                        Console.Write(num);
                    }
                    Console.Write("\n");
                }
                else if (currentSum + numArray[0] < targetNum)
                {
                    tempList.Add(numArray[0]);
                    AllSums(SubArray(numArray), targetNum, tempList);
                    AllSums(SubArray(numArray), targetNum, numList);
                }
            }
        }

        public static int[] SubArray(int[] fullArray)
        {
            var subArray = new int[fullArray.Length - 1];
            for (var i = 0; i < subArray.Length; i++)
            {
                subArray[i] = fullArray[i + 1];
            }
            return subArray;
        }
    }
}
