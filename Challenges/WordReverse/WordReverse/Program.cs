﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordReverse
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = Console.ReadLine();
            string[] sentenceSplit = sentence.Split(' ');
            foreach (var word in sentenceSplit)
            {
                var wordArray = new char[word.Length];
                for (var i = 0; i < word.Length; i++)
                {
                    wordArray[i] = word[word.Length - i - 1];
                }
                var wordString = new string(wordArray);
                Console.Write(wordString + " ");
            }
            Console.ReadLine();
        }
    }
}
