﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SWC_LMS.Models
{
    public class LmsRegisterViewModel
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string SuggestedRole { get; set; }
        public int GradeLevel { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}