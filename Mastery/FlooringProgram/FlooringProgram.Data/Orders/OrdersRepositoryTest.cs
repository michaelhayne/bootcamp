﻿using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Orders
{
    public class OrdersRepositoryTest : IOrdersRepository
    {
        public static List<Order> testList = new List<Order>()
        {
            new Order()
            {
                OrderNumber = 1,
                CustomerName = "Butta Bean",
                State = "OH",
                TaxRate = 10.00M,
                ProductType = "Wood",
                Area = 50.00M,
                CostPerSquareFoot = 5.15M,
                LaborCostPerSquareFoot = 4.75M,
                MaterialCost = 257.50M,
                LaborCost = 237.50M,
                Tax = 49.50M,
                Total = 544.50M
            },
            new Order()
            {
                OrderNumber = 2,
                CustomerName = "Boogie",
                State = "MN",
                TaxRate = 5.00M,
                ProductType = "Tile",
                Area = 15.50M,
                CostPerSquareFoot = 4.00M,
                LaborCostPerSquareFoot = 5.00M,
                MaterialCost = 62.00M,
                LaborCost = 77.50M,
                Tax = 6.975M,
                Total = 146.475M
            }
        };

        public List<string> GetDates()
        {
            string dateToday = DateTime.Now.ToString("MMddyyyy");
            var testDates = new List<string>()
            {
                dateToday
            };
            return testDates;
        }

        public void ClearRepository(string date)
        {
            testList.Clear();
        }

        public void DeleteOrder(string date, int orderNum)
        {
            var orderToRemove = testList.Single(x => x.OrderNumber == orderNum);
            testList.Remove(orderToRemove);
        }

        public void WriteOrderToRepository(Order order, string date)
        {
            testList.Add(order);
        }

        public List<Order> CreateRepository(string date)
        {
            return new List<Order>();
        }

        public bool OrderDateExists(string date)
        {
            return true;
        }

        public List<Order> GetOrders(string date)
        {
            return testList;
            //var orderList = new List<Order>()
            //{
            //    new Order()
            //    {
            //        OrderNumber = 1,
            //        CustomerName = "Butta Bean",
            //        State = "OH",
            //        TaxRate = 0.10M,
            //        ProductType = "Wood",
            //        Area = 50.00M,
            //        CostPerSquareFoot = 5.15M,
            //        LaborCostPerSquareFoot = 4.75M,
            //        MaterialCost = 257.50M,
            //        LaborCost = 237.50M,
            //        Tax = 49.50M,
            //        Total = 544.50M
            //    }
            //};

            //foreach (var order in orderList)
            //{
            //    testList.Add(order);
            //}

            //return orderList;
        }
    }
}
