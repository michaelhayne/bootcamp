﻿using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Orders
{
    public class OrdersRepositoryFactory
    {
        public static IOrdersRepository GetOrdersRepository()
        {
            switch (ConfigurationSettings.GetMode())
            {
                case "Prod":
                    return new OrdersRepositoryFile();
                case "Test":
                    return new OrdersRepositoryTest();
            }
            return null;
        }
    }
}
