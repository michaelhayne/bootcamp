﻿using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Orders
{
    public class OrdersRepositoryFile : IOrdersRepository
    {
        public List<string> GetDates()
        {
            var filePaths = Directory.GetFiles(@"Data\", "Orders_*").ToList();
            var fileNames = new List<string>();
            foreach (var path in filePaths)
            {
                string date = path.Substring(12, 8);
                fileNames.Add(date);
            }
            return fileNames;
        }

        public void ClearRepository(string date)
        {
            string filePath = "Data\\Orders_" + date + ".txt";
            File.WriteAllText(filePath, string.Empty);
            StreamWriter sw = new StreamWriter(filePath);
            sw.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
            sw.Close();
        }

        public void DeleteOrder(string date, int orderNum)
        {
            var orderListOld = GetOrders(date);
            var orderListNew = orderListOld.Where(x => x.OrderNumber != orderNum);

            string filePath = "Data\\Orders_" + date + ".txt";

            if (!orderListNew.Any())
            {
                File.Delete(filePath);
            }
            else
            {
                File.WriteAllText(filePath, string.Empty);
                StreamWriter sw = new StreamWriter(filePath, true);
                sw.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
                foreach (var order in orderListNew)
                {
                    string orderString = order.OrderNumber + "," + order.CustomerName + "," + order.State + "," + order.TaxRate + ","
                                     + order.ProductType + "," + order.Area + "," + order.CostPerSquareFoot + "," + order.LaborCostPerSquareFoot
                                     + "," + order.MaterialCost + "," + order.LaborCost + "," + order.Tax + "," + order.Total;
                    sw.WriteLine(orderString);
                }
                sw.Close();
            }
        }

        public void WriteOrderToRepository(Order order, string date)
        {
            string orderString = order.OrderNumber + "," + order.CustomerName + "," + order.State + "," + order.TaxRate + ","
                                 + order.ProductType + "," + order.Area + "," + order.CostPerSquareFoot + "," + order.LaborCostPerSquareFoot
                                 + "," + order.MaterialCost + "," + order.LaborCost + "," + order.Tax + "," + order.Total;

            string filePath = "Data\\Orders_" + date + ".txt";
            StreamWriter sw = new StreamWriter(filePath, true);
            sw.WriteLine(orderString);
            sw.Close();
        }

        public List<Order> CreateRepository(string date)
        {
            string filePath = "Data\\Orders_" + date + ".txt";
            StreamWriter sw = new StreamWriter(filePath);
            sw.WriteLine("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");
            sw.Close();
            return new List<Order>();
        }

        public bool OrderDateExists(string date)
        {
            string filePath = "Data\\Orders_" + date + ".txt";
            if (File.Exists(filePath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Order> GetOrders(string date)
        {
            string filePath = "Data\\Orders_" + date + ".txt";

            List<Order> orders = new List<Order>();
            if (File.Exists(filePath))
            {
                string[] data = File.ReadAllLines(filePath);
                for (int i = 1; i < data.Length; i++)
                {
                    string[] row = data[i].Split(',');

                    Order order = new Order();
                    order.OrderNumber = int.Parse(row[0]);
                    order.CustomerName = row[1];
                    order.State = row[2];
                    order.TaxRate = decimal.Parse(row[3]);
                    order.ProductType = row[4];
                    order.Area = decimal.Parse(row[5]);
                    order.CostPerSquareFoot = decimal.Parse(row[6]);
                    order.LaborCostPerSquareFoot = decimal.Parse(row[7]);
                    order.MaterialCost = decimal.Parse(row[8]);
                    order.LaborCost = decimal.Parse(row[9]);
                    order.Tax = decimal.Parse(row[10]);
                    order.Total = decimal.Parse(row[11]);

                    orders.Add(order);
                }
            }
            return orders;
        }
    }
}
