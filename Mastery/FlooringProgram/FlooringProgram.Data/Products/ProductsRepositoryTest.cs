﻿using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Products
{
    public class ProductsRepositoryTest : IProductsRepository
    {
        public List<Product> GetProducts()
        {
            return new List<Product>()
            {
                new Product() { ProductType = "Wood", CostPerSquareFoot = 2.45M, LaborCostPerSquareFoot = 1.50M }
            };
        }
    }
}
