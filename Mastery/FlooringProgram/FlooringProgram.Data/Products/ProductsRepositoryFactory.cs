﻿using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Products
{
    public class ProductsRepositoryFactory
    {
        public static IProductsRepository GetProductsRepository()
        {
            switch (ConfigurationSettings.GetMode())
            {
                case "Prod":
                    return new ProductsRepositoryFile();
                case "Test":
                    return new ProductsRepositoryTest();
            }
            return null;
        }
    }
}
