﻿using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Products
{
    public class ProductsRepositoryFile : IProductsRepository
    {
        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();

            string[] data = File.ReadAllLines(@"Data\Products.txt");
            for (int i = 1; i < data.Length; i++)
            {
                string[] row = data[i].Split(',');

                Product productToAdd = new Product();
                productToAdd.ProductType = row[0];
                productToAdd.CostPerSquareFoot = decimal.Parse(row[1]);
                productToAdd.LaborCostPerSquareFoot = decimal.Parse(row[2]);

                products.Add(productToAdd);
            }
            return products;
        }
    }
}
