﻿using FlooringProgram.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Operations
{
    public class LoadTextFile
    {
        public static List<Order> LoadOrderList(string filePath)
        {
            int lineNumber = 0;
            var orderList = new List<Order>();
            var sr = new StreamReader(filePath);
            bool lineIsNull = false;
            while (!lineIsNull)
            {
                string txtLine = sr.ReadLine();
                if (txtLine == null)
                {
                    lineIsNull = true;
                    break;
                }

                string[] lineArray = txtLine.Split(',');
                int orderNumber = 0;
                decimal taxRate = 0;
                decimal area = 0;
                decimal costPerSquareFoot = 0;
                decimal laborCostPerSquareFoot = 0;
                decimal materialCost = 0;
                decimal laborCost = 0;
                decimal tax = 0;
                decimal total = 0;

                bool parseSuccess = Int32.TryParse(lineArray[0], out orderNumber);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse ORDER NUMBER at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[3], out taxRate);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse TAX RATE at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[5], out area);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse AREA at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[6], out costPerSquareFoot);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse COST PER SQUARE FOOT at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[7], out laborCostPerSquareFoot);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse LABOR COST PER SQUARE FOOT at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[8], out materialCost);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse MATERIAL COST at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[9], out laborCost);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse LABOR COST at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[10], out tax);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse TAX at line {0}", lineNumber);
                }

                parseSuccess = Decimal.TryParse(lineArray[11], out total);
                if (!parseSuccess)
                {
                    Console.WriteLine("ERROR: could not parse TOTAL at line {0}", lineNumber);
                }
                Order lineOrder = new Order()
                {
                    OrderNumber = orderNumber,
                    CustomerName = lineArray[1],
                    State = lineArray[2],
                    TaxRate = taxRate,
                    ProductType = lineArray[4],
                    Area = area,
                    CostPerSquareFoot = costPerSquareFoot,
                    LaborCostPerSquareFoot = laborCostPerSquareFoot,
                    MaterialCost = materialCost,
                    LaborCost = laborCost,
                    Tax = tax,
                    Total = total
                };
                orderList.Add(lineOrder);
                lineNumber++;
            }
            sr.Close();
            return orderList;
        }
    }
}
