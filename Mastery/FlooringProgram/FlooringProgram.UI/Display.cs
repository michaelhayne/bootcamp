﻿using FlooringProgram.Operations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.UI
{
    class Display
    {
        public static void DisplayDateOrders(string date)
        {
            var dateArray = new string[3];
            dateArray[0] = date.Substring(0, 2);
            dateArray[1] = date.Substring(2, 2);
            dateArray[2] = date.Substring(4);
            string formattedDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];

            string filePath = "Data\\Orders_" + date + ".txt";
            if (File.Exists(filePath))
            {
                var orderList = LoadTextFile.LoadOrderList(filePath);
                Console.WriteLine("Orders for {0}\n---------------------", formattedDate);
                foreach (var order in orderList)
                {
                    Console.Write(order.OrderNumber + " " + order.CustomerName + " " + order.Total + "\n");
                }
            }
            else
            {
                Console.WriteLine("No orders on that date.");
            }
        }
    }
}
