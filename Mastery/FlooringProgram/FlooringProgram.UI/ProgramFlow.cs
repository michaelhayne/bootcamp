﻿using FlooringProgram.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.UI
{
    class ProgramFlow
    {
        public static void Start()
        {
            bool endProgram = false;
            while (!endProgram)
            {
                Console.Write("[1] Display orders\n[2] Add order\n[3] Delete order\n[4] Edit order\n\nEnter 1-4 or 'Q' to quit: ");
                string input = Console.ReadLine().ToUpper();
                switch (input)
                {
                    case "1":
                        Console.Clear();
                        OrderOperations.DisplayOrderDates();
                        break;
                    case "2":
                        Console.Clear();
                        OrderOperations.AddOrder();
                        break;
                    case "3":
                        Console.Clear();
                        OrderOperations.DeleteOrder();
                        break;
                    case "4":
                        Console.Clear();
                        OrderOperations.EditOrder();
                        break;
                    case "Q":
                        endProgram = true;
                        break;
                }
            }
        }
    }
}
