﻿using FlooringProgram.Data.Products;
using FlooringProgram.Data.TaxRates;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Operations
{
    public class Calculator
    {
        public static Order CalculateOrderPrices(int ordNum, string cusNam, string sta, string proTyp, decimal are)
        {
            decimal taxRate = 0;
            decimal costPerSquareFoot = 0;
            decimal laborCostPerSquareFoot = 0;
            decimal materialCost = 0;
            decimal laborCost = 0;
            decimal tax = 0;
            decimal total = 0;

            ITaxRateRepository taxRepository = TaxRateRepositoryFactory.GetTaxRateRepository();
            IProductsRepository productRepository = ProductsRepositoryFactory.GetProductsRepository();
            var taxList = taxRepository.GetTaxRates();
            var productList = productRepository.GetProducts();

            TaxRate taxInfo = taxList.Find(x => x.State == sta);
            if (taxInfo != null)
            {
                taxRate = taxInfo.TaxPercent;
            }
            else
            {
                Console.WriteLine("ERROR: Could not find tax info for {0}", sta);
            }

            Product costInfo = productList.Find(x => x.ProductType == proTyp);
            if (costInfo != null)
            {
                costPerSquareFoot = costInfo.CostPerSquareFoot;
                laborCostPerSquareFoot = costInfo.LaborCostPerSquareFoot;
            }
            else
            {
                Console.WriteLine("ERROR: Could not find product info for {0}", proTyp);
            }

            materialCost = are * costPerSquareFoot;
            laborCost = are * laborCostPerSquareFoot;
            tax = (materialCost + laborCost) * (taxRate * 0.01M);
            total = materialCost + laborCost + tax;

            Order order = new Order()
            {
                OrderNumber = ordNum,
                CustomerName = cusNam,
                State = sta,
                TaxRate = taxRate,
                ProductType = proTyp,
                Area = are,
                CostPerSquareFoot = costPerSquareFoot,
                LaborCostPerSquareFoot = laborCostPerSquareFoot,
                MaterialCost = materialCost,
                LaborCost = laborCost,
                Tax = tax,
                Total = total
            };

            return order;
        }
    }
}
