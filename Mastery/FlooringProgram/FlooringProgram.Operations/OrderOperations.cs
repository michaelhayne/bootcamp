﻿using FlooringProgram.Data.Orders;
using FlooringProgram.Data.Products;
using FlooringProgram.Data.TaxRates;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FlooringProgram.Operations
{
    public class OrderOperations
    {
        //*************************EDIT ORDER***************************************
        public static void EditOrder()
        {
            Console.Write("Date of order you wish to edit (mm/dd/yyyy): ");
            string dateNonFormatted = null;
            string dateFormatted = null;
            bool okayInput = false;
            DateTime date;

            while (!okayInput)
            {
                dateNonFormatted = Console.ReadLine();
                if (DateTime.TryParse(dateNonFormatted, out date))
                {
                    okayInput = true;
                    dateFormatted = string.Format("{0:MMddyyyy}", date);
                }
                else
                {
                    Console.Clear();
                    Console.Write("Invalid input, enter date in mm/dd/yyyy format: ");
                }
            }

            IOrdersRepository repository = OrdersRepositoryFactory.GetOrdersRepository();

            bool orderDateExists = repository.OrderDateExists(dateFormatted);

            if (orderDateExists)
            {
                Console.Clear();
                DisplayOrdersOnDate(dateFormatted);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("No orders made on {0}", dateNonFormatted);
            }

            string userInput = null;
            okayInput = false;
            var orderList = repository.GetOrders(dateFormatted);
            int[] orderNumbers = new int[orderList.Count];
            int i = 0;
            foreach (var order in orderList)
            {
                orderNumbers[i] = order.OrderNumber;
                i++;
            }

            Console.Write("\nEnter order # of order you wish to edit\nor enter R (return) to return to main menu: ");

            while (!okayInput)
            {
                int orderNum = 0;
                userInput = Console.ReadLine().ToUpper();
                bool isInt = int.TryParse(userInput, out orderNum);

                if ((userInput == "R") || (userInput == "RETURN"))
                {
                    okayInput = true;
                }
                else if (isInt && orderNumbers.Contains(orderNum))
                {
                    Order orderToEdit = orderList.Find(x => x.OrderNumber == orderNum);
                    var dateArray = new string[3];
                    dateArray[0] = dateFormatted.Substring(0, 2);
                    dateArray[1] = dateFormatted.Substring(2, 2);
                    dateArray[2] = dateFormatted.Substring(4);
                    string formattedDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];

                    Order editedOrder = EditOrderProperties(orderToEdit, formattedDate);
                    orderList = ReplaceOrderInList(orderList, editedOrder);
                    repository.ClearRepository(dateFormatted);
                    foreach (var ord in orderList)
                    {
                        repository.WriteOrderToRepository(ord, dateFormatted);
                    }
                    Console.Clear();
                    DisplayOrdersOnDate(dateFormatted);
                    Console.Write("\nEnter order # of order you wish to edit\nor enter R (return) to return to main menu: ");
                }
                else
                {
                    Console.Clear();
                    DisplayOrdersOnDate(dateFormatted);
                    Console.Write("\nInvalid input, enter an order # or R (return): ");
                }
            }
        }

        public static Order EditOrderProperties(Order orderOld, string date)
        {
            Order orderNew = new Order()
            {
                OrderNumber = orderOld.OrderNumber,
                CustomerName = orderOld.CustomerName,
                State = orderOld.State,
                ProductType = orderOld.ProductType,
                Area = orderOld.Area
            };

            bool stopEdit = false;

            while (!stopEdit)
            {
                Console.Clear();
                Console.WriteLine("{0}   Order #{1}\n---------------------------", date, orderNew.OrderNumber);
                Console.WriteLine("{0,-16}{1}", "[1] Customer:", orderNew.CustomerName);
                Console.WriteLine("{0,-16}{1}", "[2] State:", orderNew.State);
                Console.WriteLine("{0,-16}{1}", "[3] Product:", orderNew.ProductType);
                Console.WriteLine("{0,-16}{1}", "[4] Area:", orderNew.Area);
                Console.Write("\nEnter 1-4 to edit the order, S to save the order\n=> ");
                string input = Console.ReadLine().ToUpper();

                bool okayInput = false;

                switch (input)
                {
                    case "1":
                        while (!okayInput)
                        {
                            Console.Clear();
                            Console.Write("Customer: ");
                            string customerName = Console.ReadLine();
                            if (string.IsNullOrWhiteSpace(customerName))
                            {
                                Console.Clear();
                                Console.Write("Customer: ");
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("[INVALID NAME]");
                                Console.ResetColor();
                                Console.ReadLine();
                            }
                            else
                            {
                                orderNew.CustomerName = customerName;
                                okayInput = true;
                            }
                        }
                        break;
                    case "2":
                        while (!okayInput)
                        {
                            Console.Clear();
                            Console.Write("State: ");
                            string state = Console.ReadLine();
                            if (!TaxRateOperations.IsAllowedState(state))
                            {
                                Console.Clear();
                                Console.Write("State: ");
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("[INVALID STATE]");
                                Console.ResetColor();
                                Console.ReadLine();
                            }
                            else
                            {
                                orderNew.State = state;
                                okayInput = true;
                            }
                        }
                        break;
                    case "3":
                        while (!okayInput)
                        {
                            Console.Clear();
                            Console.Write("Product: ");
                            string productType = Console.ReadLine();
                            productType = productType.First().ToString().ToUpper() + string.Join("", productType.ToLower().Skip(1));
                            if (!ProductOperations.IsAllowedProduct(productType))
                            {
                                Console.Clear();
                                Console.Write("Product: ");
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("[INVALID PRODUCT]");
                                Console.ResetColor();
                                Console.ReadLine();
                            }
                            else
                            {
                                orderNew.ProductType = productType;
                                okayInput = true;
                            }
                        }
                        break;
                    case "4":
                        while (!okayInput)
                        {
                            Console.Clear();
                            Console.Write("Area: ");

                            decimal area = 0;
                            string areaString = Console.ReadLine();
                            bool isDecimal = decimal.TryParse(areaString, out area);

                            if (!isDecimal)
                            {
                                Console.Clear();
                                Console.Write("Area: ");
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("[INVALID AREA]");
                                Console.ResetColor();
                                Console.ReadLine();
                            }
                            else
                            {
                                orderNew.Area = area;
                                okayInput = true;
                            }
                        }
                        break;
                    case "S":
                        orderNew = Calculator.CalculateOrderPrices(orderNew.OrderNumber, orderNew.CustomerName, orderNew.State, orderNew.ProductType, orderNew.Area);
                        stopEdit = true;
                        break;
                }
            }

            return orderNew;
        }
        //*************************END OF EDIT ORDER********************************

        //*************************DELETE ORDER*************************************
        public static void DeleteOrder()
        {
            Console.Write("Date of order you wish to remove (mm/dd/yyyy): ");
            string dateNonFormatted = null;
            string dateFormatted = null;
            bool okayInput = false;
            DateTime date;

            while (!okayInput)
            {
                dateNonFormatted = Console.ReadLine();
                if (DateTime.TryParse(dateNonFormatted, out date))
                {
                    okayInput = true;
                    dateFormatted = string.Format("{0:MMddyyyy}", date);
                }
                else
                {
                    Console.Clear();
                    Console.Write("Invalid input, enter date in mm/dd/yyyy format: ");
                }
            }

            IOrdersRepository repository = OrdersRepositoryFactory.GetOrdersRepository();

            bool orderDateExists = repository.OrderDateExists(dateFormatted);

            if (orderDateExists)
            {
                Console.Clear();
                DisplayOrdersOnDate(dateFormatted);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("No orders made on {0}", dateNonFormatted);
            }

            string userInput = null;
            okayInput = false;
            var orderList = repository.GetOrders(dateFormatted);
            int[] orderNumbers = new int[orderList.Count];
            int i = 0;
            foreach (var order in orderList)
            {
                orderNumbers[i] = order.OrderNumber;
                i++;
            }

            Console.Write("\nEnter order # of order you wish to delete\nor enter R (return) to return to main menu: ");

            while (!okayInput)
            {
                int orderNum = 0;
                userInput = Console.ReadLine().ToUpper();
                bool isInt = int.TryParse(userInput, out orderNum);

                if ((userInput == "R") || (userInput == "RETURN"))
                {
                    okayInput = true;
                }
                else if (isInt && orderNumbers.Contains(orderNum))
                {
                    repository.DeleteOrder(dateFormatted, orderNum);
                    okayInput = true;
                }
                else
                {
                    Console.Clear();
                    DisplayOrdersOnDate(dateFormatted);
                    Console.Write("\nInvalid input, enter an order # or R (return): ");
                }
            }
        }

        public static void AddOrder()
        {
            List<Order> orderList = new List<Order>();
            int orderNumber = 1;

            // Load repository for today's date if it exists, otherwise make a new one
            string dateToday = DateTime.Now.ToString("MMddyyyy");

            IOrdersRepository repository = OrdersRepositoryFactory.GetOrdersRepository();

            bool orderDateExists = repository.OrderDateExists(dateToday);

            if (orderDateExists)
            {
                orderList = repository.GetOrders(dateToday);
                int[] orderNumbers = new int[orderList.Count];
                int i = 0;
                foreach (var order in orderList)
                {
                    orderNumbers[i] = order.OrderNumber;
                    i++;
                }
                orderNumber = orderNumbers.Max() + 1;
            }
            //else
            //{
            //    orderList = repository.CreateRepository(dateToday);
            //}

            ITaxRateRepository taxRepository = TaxRateRepositoryFactory.GetTaxRateRepository();
            var stateList = taxRepository.GetTaxRates();
            var stateArray = new string[stateList.Count];
            int j = 0;
            foreach (var stateTax in stateList)
            {
                stateArray[j] = stateTax.State;
                j++;
            }

            IProductsRepository productRepository = ProductsRepositoryFactory.GetProductsRepository();
            var productList = productRepository.GetProducts();
            var productArray = new string[productList.Count];
            int k = 0;
            foreach (var prod in productList)
            {
                productArray[k] = prod.ProductType;
                k++;
            }

            Console.Write("Customer name: ");
            string customerName = null;
            bool validName = false;
            while (!validName)
            {
                customerName = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(customerName))
                {
                    validName = true;
                }
                else
                {
                    Console.Clear();
                    Console.Write("Customer name: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[INVALID NAME]\n");
                    Console.ResetColor();
                    Console.ReadLine();
                    Console.Clear();
                    Console.Write("Customer name: ");
                }
            }

            Console.Write("State: ");
            string state = null;
            bool validState = false;
            while (!validState)
            {
                state = Console.ReadLine().ToUpper();
                if (stateArray.Contains(state))
                {
                    validState = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Customer name: {0}", customerName);
                    Console.Write("State: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[INVALID STATE]\n");
                    Console.ResetColor();
                    Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Customer name: {0}", customerName);
                    Console.Write("State: ");
                }
            }

            Console.Write("Product type: ");
            string productType = null;
            bool validProduct = false;
            while (!validProduct)
            {
                productType = Console.ReadLine();
                productType = productType.First().ToString().ToUpper() + string.Join("", productType.ToLower().Skip(1));
                if (productArray.Contains(productType))
                {
                    validProduct = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Customer name: {0}", customerName);
                    Console.WriteLine("State: {0}", state);
                    Console.Write("Product type: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[INVALID PRODUCT]\n");
                    Console.ResetColor();
                    Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Customer name: {0}", customerName);
                    Console.WriteLine("State: {0}", state);
                    Console.Write("Product type: ");
                }
            }

            Console.Write("Area (sq ft): ");
            decimal area = 0;
            bool validArea = false;
            while (!validArea)
            {
                validArea = decimal.TryParse(Console.ReadLine(), out area);
                if (!validArea)
                {
                    Console.Clear();
                    Console.WriteLine("Customer name: {0}", customerName);
                    Console.WriteLine("State: {0}", state);
                    Console.WriteLine("Product type: {0}", productType);
                    Console.Write("Area: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("[INVALID AREA]\n");
                    Console.ResetColor();
                    Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Customer name: {0}", customerName);
                    Console.WriteLine("State: {0}", state);
                    Console.WriteLine("Product type: {0}", productType);
                    Console.Write("Area: ");
                }
            }

            Order orderToAdd = Calculator.CalculateOrderPrices(orderNumber, customerName, state, productType, area);

            // Check to commit order
            Console.Clear();
            DisplayOrder(orderToAdd);
            Console.Write("\n\nPlace order? (Y or N): ");
            bool okayInput = false;
            while (!okayInput)
            {
                string response = Console.ReadLine().ToUpper();
                if ((response == "Y") || (response == "YES"))
                {
                    if (!orderDateExists)
                    {
                        orderList = repository.CreateRepository(dateToday);
                    }
                    repository.WriteOrderToRepository(orderToAdd, dateToday);
                    okayInput = true;
                }
                else if ((response == "N") || (response == "NO"))
                {
                    okayInput = true;
                }
                else
                {
                    Console.Clear();
                    DisplayOrder(orderToAdd);
                    Console.Write("\n\nPlease enter Y (yes) or N (no): ");
                }
            }
        }

        public static void DisplayOrderDates()
        {
            string dateToDisplay = null;
            int inputErrorNumber = 0;
            bool stopDisplay = false;
            while (!stopDisplay)
            {
                Console.Clear();
                Console.Write("Dates of orders\n---------------\n");
                IOrdersRepository repository = OrdersRepositoryFactory.GetOrdersRepository();
                var dates = repository.GetDates();
 
                var orderedDateList = new List<string>();

                foreach (var date in dates)
                {
                    var dateArray = new string[3];
                    dateArray[0] = date.Substring(0, 2);
                    dateArray[1] = date.Substring(2, 2);
                    dateArray[2] = date.Substring(4);

                    string arrangedDate = dateArray[2] + dateArray[0] + dateArray[1];
                    orderedDateList.Add(arrangedDate);
                }

                orderedDateList = orderedDateList.OrderBy(x => int.Parse(x)).ToList();

                foreach (var date in orderedDateList)
                {
                    var dateArray = new string[3];
                    dateArray[0] = date.Substring(4, 2);
                    dateArray[1] = date.Substring(6);
                    dateArray[2] = date.Substring(0, 4);

                    string formattedDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];

                    Console.WriteLine(formattedDate);
                }

                if (inputErrorNumber == 1)
                {
                    var dateArray = new string[3];
                    dateArray[0] = dateToDisplay.Substring(0, 2);
                    dateArray[1] = dateToDisplay.Substring(2, 2);
                    dateArray[2] = dateToDisplay.Substring(4);
                    string formattedDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("\nNo orders on {0}", formattedDate);
                    Console.ResetColor();
                    Console.Write("\nEnter new date or 'R': ");
                }
                else if (inputErrorNumber == 2)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("\nInvalid input, enter new date: ");
                    Console.ResetColor();
                    Console.Write("\nEnter new date or 'R': ");
                }
                else
                {
                    Console.Write("\nEnter date (MM/DD/YYYY) to view orders: ");
                }

                DateTime dateInput;

                string rawInput = Console.ReadLine();
                if (rawInput.ToUpper() == "R")
                {
                    Console.Clear();
                    stopDisplay = true;
                }
                else if (DateTime.TryParse(rawInput, out dateInput))
                {
                    dateToDisplay = string.Format("{0:MMddyyyy}", dateInput);
                    if (dates.Contains(dateToDisplay))
                    {
                        inputErrorNumber = 0;
                        Console.Clear();
                        DisplayOrdersOnDate(dateToDisplay);
                        Console.ReadLine();
                    }
                    else
                    {
                        inputErrorNumber = 1;
                    }
                }
                else
                {
                    inputErrorNumber = 2;
                }
            }
        }

        public static void DisplayOrdersOnDate(string date)
        {
            // Format date string to look like a date (EX: 02231987 => 02/23/1987)
            var dateArray = new string[3];
            dateArray[0] = date.Substring(0, 2);
            dateArray[1] = date.Substring(2, 2);
            dateArray[2] = date.Substring(4);
            string formattedDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];

            IOrdersRepository repository = OrdersRepositoryFactory.GetOrdersRepository();
            var orderList = repository.GetOrders(date);

            Console.WriteLine("Orders for {0}\n-----------------------------------------------", formattedDate);
            Console.WriteLine("{0,-3}{1,-14}{2,-10}{3,-10}{4,-10}\n-----------------------------------------------", "#", "Customer", "Product", "Area", "Total");
            foreach (var order in orderList)
            {
                Console.Write("{0,-3}{1,-14}{2,-10}{3,-10}{4,-10:C}\n", order.OrderNumber, order.CustomerName, order.ProductType, order.Area, order.Total);
            }
        }

        public static void DisplayOrder(Order order)
        {
            Console.Write("Customer:  {0}", order.CustomerName);
            Console.Write("\nState:     {0} ({1}% tax)", order.State, order.TaxRate);
            Console.Write("\nProduct:   {0} ({1:C} sq ft)", order.ProductType, order.CostPerSquareFoot + order.LaborCostPerSquareFoot);
            Console.Write("\nArea:      {0} sq ft", order.Area);
            Console.Write("\nTotal:     {0:C}", order.Total);
        }

        public static List<Order> ReplaceOrderInList(List<Order> listOld, Order orderNew)
        {
            var listTemp = listOld.Where(x => x.OrderNumber != orderNew.OrderNumber).ToList();
            listTemp.Add(orderNew);
            var listNew = listTemp.OrderBy(x => x.OrderNumber).ToList();
            return listNew;
        }
    }
}
