﻿using FlooringProgram.Data.Products;
using FlooringProgram.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Operations
{
    public class ProductOperations
    {
        public static bool IsAllowedProduct(string productType)
        {
            IProductsRepository repository = ProductsRepositoryFactory.GetProductsRepository();
            var allProducts = repository.GetProducts();

            return allProducts.Any(p => p.ProductType == productType);
        }
    }
}
