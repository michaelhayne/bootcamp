﻿using FlooringProgram.Data.Orders;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.Operations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlooringProgram.Operations.Helper
{
    public class Edit
    {
        public static void EditOptions(string date, int orderID)
        {
            //find order 
            IOrdersRepository change = OrdersRepositoryFactory.GetOrdersRepository();
            bool orderDateExists = change.OrderDateExists(date);

            //create list
            List<Order> getOrder = new List<Order>();
            getOrder = change.GetOrders(date);

            //declare current values from list
            //I am sure there is a simpler way to do this, but this works.
            int orderNumber = orderID;
            string oldName = getOrder.FirstOrDefault(x => x.OrderNumber == orderID).CustomerName;
            string oldState = getOrder.FirstOrDefault(x => x.OrderNumber == orderID).State;
            string oldProd = getOrder.FirstOrDefault(x => x.OrderNumber == orderID).ProductType;
            decimal oldArea = getOrder.FirstOrDefault(x => x.OrderNumber == orderID).Area;


            string customerName = oldName;
            string state = oldState;
            string productType = oldProd;
            decimal area = oldArea;

            //print current values
            PrintList(customerName, state, productType, area);


            //name prompt
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\tChange Name? (Y or N): ");
            Console.ResetColor();
            string verifyname = Console.ReadLine().ToUpper();
            string answer1 = Utilities.AnswerValid(verifyname);

            string n;

            if (answer1 == "Y")
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\tPlease enter the new name: ");
                Console.ResetColor();
                n = Console.ReadLine();
                customerName = Utilities.NameValidate(n);
            }
            else if (answer1 == "N")
            {
                AnyKey();
            }

            //state prompt
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\tChange State? (Y or N): ");
            Console.ResetColor();
            string verifystate = Console.ReadLine().ToUpper();
            string answer2 = Utilities.AnswerValid(verifystate);

            string st;

            if (answer2 == "Y")
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\tThe taxable states are: OH, MI, IN, and PA.");
                Console.Write("\tPlease enter the new state: ");
                Console.ResetColor();
                st = Console.ReadLine().ToUpper();
                state = Utilities.StateValidate(st);
            }
            else if (answer2 == "N")
            {
                AnyKey();
            }

            //product prompt
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\tChange Product Type? (Y or N): ");
            Console.ResetColor();
            string verifyprod = Console.ReadLine().ToUpper();
            string answer3 = Utilities.AnswerValid(verifyprod);

            string pt;

            if (answer3 == "Y")
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\tThe product types are: Carpet, Laminate, Tile, and Wood.");
                Console.Write("\tPlease enter the new product type: ");
                Console.ResetColor();
                pt = Console.ReadLine().ToLower();
                productType = Utilities.ProductValidate(pt);
            }
            else if (answer3 == "N")
            {
                AnyKey();
            }

            //area prompt
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\tChange Area (sq ft)? (Y or N): ");
            Console.ResetColor();
            string verifyarea = Console.ReadLine().ToUpper();
            string answer4 = Utilities.AnswerValid(verifyarea);

            string a;

            if (answer4 == "Y")
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\tPlease enter the new area: ");
                Console.ResetColor();
                a = Console.ReadLine();
                area = decimal.Parse(a);
            }
            else if (answer4 == "N")
            {
                AnyKey();
            }


            Order updateOrder = Calculator.CalculateOrderPrices(orderNumber, customerName, state, productType, area);

            //print changed values
            Console.Clear();
            Edit.EditHeader();
            Console.ForegroundColor = ConsoleColor.Yellow;
            OrderOperations.DisplayOrder(updateOrder);
            Console.ResetColor();
            Console.WriteLine();

            // ask if commit changes
            Console.WriteLine();
            Console.Write("\tSave changes? (Y or N): ");
            bool commitValid = true;
            while (commitValid)
            {
                string response = Console.ReadLine().ToUpper();
                string answer5 = Utilities.AnswerValid(response);

                if (answer5 == "Y")
                {
                    //getOrder = change.CreateRepository(date);
                    var updatedList = getOrder.Where(y => y.OrderNumber != updateOrder.OrderNumber).ToList();
                    updatedList.Add(updateOrder);
                    updatedList = updatedList.OrderBy(z => z.OrderNumber).ToList();
                    change.ClearRepository(date);
                    foreach (var ord in updatedList)
                    {
                        change.WriteOrderToRepository(ord, date);
                    }
                    //change.WriteOrderToRepository(updateOrder, date);
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\tYour changes were saved. Press any key to return to the main menu.");
                    Console.ResetColor();
                    Console.ReadKey();
                    Console.Clear();
                    OrderOperations.MainMenu();
                }
                else if (answer5 == "N")
                {
                    //revert back
                    customerName = oldName;
                    state = oldState;
                    productType = oldProd;
                    area = oldArea;
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Nothing was changed. Press any key to return to the main menu.");
                    Console.ResetColor();
                    Console.ReadKey();
                    Console.Clear();
                    OrderOperations.MainMenu();
                    commitValid = false;
                }

                //NOT FINISHED
                //STILL NEED TO MERGE AND TIE IN ALL FUNCTIONS TO MENUS
            }
        }

        public static void PrintList(string customerName, string state, string productType, decimal area)
        {
            Console.Clear();
            Edit.EditHeader();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\tCurrent Name: {0}", customerName);
            Console.WriteLine("\tCurrent State: {0}", state);
            Console.WriteLine("\tCurrent ProductType: {0}", productType);
            Console.WriteLine("\tCurrent Area (sq. ft.): {0}", area);
            Console.ResetColor();
        }



        public static void AnyKey()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("\tPress any key to continue.");
            Console.ResetColor();
            Console.ReadKey();
            Console.WriteLine();
        }



        public static void EditHeader()
        {
            Utilities.Header();
            //menu header 
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\t\t\t\t   EDIT ORDER");
            Console.ResetColor();
            Console.WriteLine();
        }
    }
}
