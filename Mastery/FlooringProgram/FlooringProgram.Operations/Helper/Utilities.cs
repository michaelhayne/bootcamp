﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FlooringProgram.Operations.Helper
{
    public class Utilities
    {
        public static void Header()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            string hr = @"
        ====================================================================

                                    SWC CORP
                              FLOORING ORDERS MENU

        ====================================================================
";
            Console.WriteLine(hr);
            Console.ResetColor();
        }

        public static void DeleteHeader()
        {
            Header();
            //menu header 
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\t\t\t\t   DELETE ORDER");
            Console.ResetColor();
            Console.WriteLine();
        }

        public static void DisplayHeader()
        {
            Header();
            //menu header 
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\t\t\t\t DISPLAY ORDER");
            Console.ResetColor();
            Console.WriteLine();
        }

        public static void AddHeader()
        {
            Header();
            //menu header 
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\t\t\t\t   ADD ORDER");
            Console.ResetColor();
            Console.WriteLine();
        }

        public static string FormatDate(string date)
        {
            var dateArray = new string[3];
            dateArray[0] = date.Substring(0, 2);
            dateArray[1] = date.Substring(2, 2);
            dateArray[2] = date.Substring(4);
            string formattedDate = dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2];
            return formattedDate;
        }

        public static int IsIntValid(string input)
        {
            int number;
            while (!int.TryParse(input, out number) || string.IsNullOrEmpty(input))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\tInvalid response. Please try again: ");
                Console.ResetColor();
                input = Console.ReadLine();
            }
            return number;
        }

        public static string SentenceCase(string input)
        {
            if (input.Length < 1)
                return input;

            string sentence = input.ToLower();
            return sentence[0].ToString().ToUpper() +
               sentence.Substring(1);
        }

        public static string ProductValidate(string input)
        {
            while (!Regex.Match(input, @"^[a-z]+$").Success || string.IsNullOrEmpty(input))
            {

                if (input != "carpet" || input != "laminate" || input != "tile" || input != "wood")
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("\tInvalid entry. Please enter the new product type: ");
                    Console.ResetColor();
                    input = Console.ReadLine();
                    string lower = input.ToLower();
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\tInvalid response. Please try again: ");
                Console.ResetColor();
                input = Console.ReadLine();
            }
            string capitalize = SentenceCase(input);
            input = capitalize;
            return input;
        }

        public static string StateValidate(string input)
        {
            while (!Regex.Match(input, @"^[a-zA-Z]+$").Success || string.IsNullOrEmpty(input) || input.Length != 2)
            {
                if (input != "OH" || input != "PA" || input != "MI" || input != "IN")
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("\tInvalid state. Please enter OH, PA, MI, or IN: ");
                    Console.ResetColor();
                    input = Console.ReadLine().ToUpper();
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\tInvalid entry. Please enter OH, PA, MI, or IN: ");
                Console.ResetColor();
                input = Console.ReadLine();
            }
            return input;
        }

        public static string NameCheck(string input)
        {
            while (!Regex.Match(input, @"^[A-Za-z\s-_]+$").Success || string.IsNullOrEmpty(input))
            {
                Console.Clear();
                Utilities.AddHeader();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tCustomer Name: Invalid response. Please try again.");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\tCustomer Name: ");
                Console.ResetColor();
                input = Console.ReadLine();

            }
            string capitalize = SentenceCase(input);
            input = capitalize;
            return input;
        }

        public static string NameValidate(string input)
        {
            while (!Regex.Match(input, @"^[A-Za-z\s-_]+$").Success || string.IsNullOrEmpty(input))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\tInvalid response. Please try again: ");
                Console.ResetColor();
                input = Console.ReadLine();

            }
            string capitalize = SentenceCase(input);
            input = capitalize;
            return input;
        }

        public static string AnswerValid(string input)
        {
            while (!Regex.Match(input, @"^[nyNY]+$").Success || input.Length > 1 || string.IsNullOrEmpty(input))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\tPlease respond with a Y or N: ");
                Console.ResetColor();
                input = Console.ReadLine();
                string upper = input.ToUpper();
            }
            return input;
        }
    }
}
