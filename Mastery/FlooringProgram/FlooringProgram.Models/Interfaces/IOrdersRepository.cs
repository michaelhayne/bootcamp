﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models.Interfaces
{
    public interface IOrdersRepository
    {
        List<string> GetDates();

        List<Order> GetOrders(string date);

        List<Order> CreateRepository(string date);

        void ClearRepository(string date);

        void DeleteOrder(string date, int orderNum);

        void WriteOrderToRepository(Order order, string date);

        bool OrderDateExists(string date);
    }
}
